#!/usr/bin/env python

"""
Patch the existing scripts and either create new files with new settings or patch up the existing files
"""

import os
import sys
import re

def patch(dir, file_name_pattern = "*", pattern, text, create_new = False, postfix = None):
	for file in os.listdir(dir):
		 if not file.startswith('.') and re.match(file_name_pattern, file):
			print(file)

	if (False):
	    with open(filename) as ifp:
	        cpg = re.match(".*_(C.*)_meth.bed", filename).group(1)


	        for line in ifp:
	            line = line.rstrip('\n')
	            if not line or line.startswith('#'): continue

	            tokens = line.split('\t')
	            chrom, start, end, meth  = tokens[0:4]

	            n_m, n_total = re.match("^'(\d+)/(\d+)", meth).groups()
	            n_um = int(n_total) - int(n_m)

	            print('\t'.join(map(str, (chrom, start, cpg, n_m, n_um))))


def parse_args(args):
    from argparse import ArgumentParser
    description = __doc__.strip()

    parser = ArgumentParser(description=description)
    parser.add_argument('--dir', help='Directory with files to be patched up')
    parser.add_argument('--file_name_pattern', help='The pattern to match the filename where the replacement should be made')
    parser.add_argument('--pattern', help='The pattern to match the text that should be replaced.')
    parser.add_argument('--text', help='With what to replace')
    parser.add_argument('--create-new', default=False, action='store_true')
    parser.add_argument('--postfix', help='If new files should be created, add postfix to filenames.')


    return parser.parse_args(args)

def main(args=sys.argv[1:]):
    args = parse_args(args)
    patch(**vars(args))

if __name__ == '__main__':
    sys.exit(main())