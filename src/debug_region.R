setwd("/Users/yulia/Documents/imprinting_project/")

source("src/helper_functions.R")
source("src/find_bumps.R")
source("src/Compute_candidates_for_various_data_subsets.R")
source("src/Get_final_candidates.R")

library(doParallel)
closeAllConnections()
cl <- makeCluster(2)
registerDoParallel(cl)
getDoParWorkers()


load("saved_data/For_computing_roc_curve.updated2.RData")
load("saved_data/for_bump_hunting.RData")
load("saved_data/sets_of_train_test.reduced.RData")

sample_description$Cell_Type <- sapply(sample_description$Cell_Type, toString)
sample_description$Cell_Type[grepl("BA-*", sample_description$Cell_Type)] <- "Brain"

CONTROL_TISSUE <- "Placenta"
MODIFIED_BH <- TRUE

FILE_PREFIX <- "bumphunter_results.modified."
FILE_POSTFIX <- ".005.RData"

DIR_OUTPUT <- "modified_bumphunting_results_fmeasure_2_36_known_regions_bump_dividing/"
DIR_BUMPS <- "saved_data/modified_bump_hunting_results_ks_threshold/"

PREFIX <- paste0(DIR_OUTPUT,  CONTROL_TISSUE, "/")
DIR_BUMPS <- paste0(DIR_BUMPS,  CONTROL_TISSUE, "/")

MODEL_NAME <-  "nullmodel_permutations_jointly_distr_direction"

prefix <- get_prefix(PREFIX, MODIFIED_BH, MODEL_NAME)

load(paste0(prefix, "training_results.RData"))

optimal_parameters <- merge_optimal_parameters(results_train_test_sets)  

params <- as.list(unlist(optimal_parameters))

dmr_candidates_preliminary <- list()
for (exclude_anchm in c(0,1))
{
  file <- paste0(DIR_BUMPS, FILE_PREFIX, MODEL_NAME, ifelse(exclude_anchm, "", ".with_anchm"), FILE_POSTFIX)

  unique_name = paste0("_", MODEL_NAME, "_", CONTROL_TISSUE, "_", 
                       exclude_anchm, "_", abs(ceiling(rnorm(1) * 10000)))
  
  dmr_candidates_preliminary[[exclude_anchm+1]] <- prepare_bumps(file, "tmp/", index, 
                                                                 beta_filtered.splitted, beta_filtered.annot.splitted.reduced, alleles, 
                                                                 sample_description, exclude_anchm, params, 
                                                                 sample_names, unique_name=unique_name,  trace_region = "GNASXL")
}

list[dmr_candidates.with_anchm,
     dmr_candidates.exclude_anchm,
     imprinted_positions,
     found_among_known] <- compute_candidates_for_various_data_subsets(
               dmr_candidates_preliminary[[1]],
               dmr_candidates_preliminary[[2]],
               nrow(beta_filtered),
               known_to_evaluate = known_train_test, DMR_description_to_evaluate = DMR_description_train_test, 
               known_train_test, DMR_description_train_test, prefix= prefix,
               beta_filtered.splitted, beta_filtered.annot.reduced,
               beta_filtered.annot.splitted.reduced, 
               alleles, 
               CONTROL_TISSUE, params, save_results = F, sample_names, trace_region = "GNASAS")

  known_to_evaluate = known_train_test
  found_among_known_to_evaluate <- found_among_known[found_among_known[,1] %in% names(known_to_evaluate),]
  
  # Based on regions, not CpGs
  list[true_positives, test_positives, false_positives_regions, sensitivity_tpr, fdr] <- 
    evaluate_performance_in_region_units(known_to_evaluate, dmr_candidates.combined)
  
  list[positives, true_positives, negatives, test_positives, false_positives, true_negatives, sensitivity_tpr, specificity_tnr, fdr, fpr, accuracy] <- 
    evaluate_performance(known_to_evaluate, found_among_known_to_evaluate, nrow(beta_filtered_), dmr_candidates.combined, beta_filtered.annot)