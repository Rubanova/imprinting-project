# =================================================================================
# Gather tissue report 

setwd("/Users/yulia/Documents/imprinting_project")

MODEL_WITH_CANDIDATES <- "nullmodel_permutations_jointly_distr_direction"
DIR_OUTPUT <- "modified_bumphunting_results_fmeasure_2_36_known_regions_GM_2_cpg//"
  
load("saved_data/for_bump_hunting.RData")
load("saved_data/For_computing_roc_curve.updated2.RData")
sample_description$Cell_Type <- sapply(sample_description$Cell_Type, toString)
sample_description$Cell_Type[grepl("BA-*", sample_description$Cell_Type)] <- "Brain"
tissues <- sample_description$Cell_Type
tissue_types <- unique(tissues[sample_description$Diagnosis == "Control"])
tissue_types <- tissue_types[-which(tissue_types == "Urine")]

found_among_known_total <- NULL
for (tissue in tissue_types)
{
  DIR <- paste0(DIR_OUTPUT, tissue, "/", MODEL_WITH_CANDIDATES, "/")
  FILE <- paste0("found_among_known.total.", tissue, "_controls.csv")
  
  found_among_known <- read.csv(paste0(DIR, FILE))
  
  if(is.null(found_among_known_total))
  {
    found_among_known_total <- found_among_known[,c(1,3,4)]
    colnames(found_among_known_total) <- c("Imprinted_region", "CpGs_in_DMR", "Known_or_Candidate")
  }
  found_among_known_total <- cbind(found_among_known_total, found_among_known[,2]/found_among_known_total$CpGs_in_DMR)
  colnames(found_among_known_total)[ncol(found_among_known_total)] <- tissue
}

write.csv(found_among_known_total, file=paste0(DIR_OUTPUT, "found_among_known.all_tissues.csv"), row.names=F)

