# Perform the postprocessing on the results which were obtained with and without AnCHM. Take a union of them.

compute_candidates_for_various_data_subsets <- function( dmr_candidates_preliminary.with_anchm,
                                                         dmr_candidates_preliminary.exclude_anchm,
                                                         n_cpgs, known_to_evaluate, DMR_description_to_evaluate, 
                                                         known_train_test, DMR_description_train_test, 
                                                         prefix,  beta_filtered.splitted_, beta_filtered.annot_, 
                                                         beta_filtered.annot.splitted_, alleles, 
                                                         CONTROL_TISSUE, params, save_results,  
                                                         sample_names, chrs=NULL, fill_log=F, 
                                                         trace_region = NULL)
{  
  
#   beta_filtered_ <- beta_filtered[beta_filtered.annot.reduced$CHR %in% set$train_negatives,];
#   known_to_evaluate = known_train; DMR_description_to_evaluate = DMR_description_train;
#  prefix <- "tmp/";
#  beta_filtered.splitted_ <- beta_filtered.splitted[names(beta_filtered.splitted) %in% set$train_negatives];
#  beta_filtered.annot_ <- beta_filtered.annot.reduced[beta_filtered.annot.reduced$CHR %in% set$train_negatives,];
#  beta_filtered.annot.splitted_ <- beta_filtered.annot.splitted.reduced[names(beta_filtered.annot.splitted.reduced) %in% set$train_negatives]; 
#  save_results = FALSE; chrs = set$train_negatives;

  exclude_anchm <- 0
  
  dir.create(prefix, showWarnings = F, recursive = T)
  
  list[dmr_candidates.with_anchm, found_among_known, 
       positives, true_positives, 
       negatives, test_positives, 
       false_positives, true_negatives, 
       sensitivity_tpr, specificity_tnr, 
       fdr, fpr, bump_log_with_anchm] <- filter_bumps(dmr_candidates_preliminary.with_anchm,
                                                      n_cpgs, 
                                                      known_train_test, 
                                                      DMR_description_train_test,
                                                      beta_filtered.annot_, 
                                                      exclude_anchm, params, 
                                                      sample_names, fill_log=fill_log,
                                                      trace_region = trace_region)
  
  if (save_results)
  {
    found_among_known_to_evaluate <- found_among_known[found_among_known[,1] %in% names(known_to_evaluate),]
    error_msg <- paste0(toString(params), toString(chrs), exclude_anchm, CONTROL_TISSUE)

    save_candidates(dmr_candidates.with_anchm, prefix, "with_anchm", CONTROL_TISSUE, known_to_evaluate,
                found_among_known_to_evaluate, DMR_description_to_evaluate, error_msg)
  }


  exclude_anchm <- 1
  
  list[dmr_candidates.exclude_anchm, found_among_known, 
       positives, true_positives, 
       negatives, test_positives, 
       false_positives, true_negatives, 
       sensitivity_tpr, specificity_tnr, 
       fdr, fpr, bump_log_exclude_anchm] <- filter_bumps(dmr_candidates_preliminary.exclude_anchm,
                                                         n_cpgs, 
                                                         known_train_test, 
                                                         DMR_description_train_test,
                                                         beta_filtered.annot_, 
                                                         exclude_anchm, params, 
                                                         sample_names, fill_log=fill_log,
                                                         trace_region = trace_region)
    
  if(save_results)
  {
    found_among_known_to_evaluate <- found_among_known[found_among_known[,1] %in% names(known_to_evaluate),]
    error_msg <- paste0(toString(params), toString(chrs), exclude_anchm, CONTROL_TISSUE)
  
    save_candidates(dmr_candidates.exclude_anchm, prefix, "exclude_anchm", CONTROL_TISSUE, known_to_evaluate,
                  found_among_known_to_evaluate, DMR_description_to_evaluate, error_msg)
  }

  # Combining results with and without AnCHM
  dmr_candidates.combined <- rbind(dmr_candidates.exclude_anchm, dmr_candidates.with_anchm)
  
  if (nrow(dmr_candidates.combined) == 0)
    print("Alert: no candidates found")
  
  list[dmr_candidates.combined, found_among_known] <- 
    annotate_candidates(dmr_candidates.combined, DMR_description_train_test, 
                        beta_filtered.annot.splitted_, CONTROL_TISSUE=CONTROL_TISSUE,
                        beta_filtered.splitted_, save=F, tmp_dir=prefix)
  
  found_among_known_to_evaluate <- found_among_known[found_among_known[,1] %in% names(known_to_evaluate),]
  
  list[positives, true_positives, negatives, test_positives,
       false_positives, true_negatives, sensitivity_tpr, specificity_tnr, fdr, fpr, accuracy] <- 
    evaluate_performance(known_to_evaluate, found_among_known_to_evaluate, n_cpgs, dmr_candidates.combined, beta_filtered.annot_)
  
  if(save_results)
  {
    file_name <- add_postfix(paste0(prefix, "dmr_candidates.combined.all_columns"), !is.null(CONTROL_TISSUE), paste0(".",CONTROL_TISSUE, "_controls"))
    make_bed_file_regions(dmr_candidates.combined, file_name, store_other_columns = T)
    
    dmr_candidates.combined.known <- dmr_candidates.combined[dmr_candidates.combined$score != 0, ]
    file_name <- add_postfix(paste0(prefix, "dmr_candidates.combined.known"), !is.null(CONTROL_TISSUE), paste0(".",CONTROL_TISSUE, "_controls"))
    make_bed_file_regions(dmr_candidates.combined.known, file_name, store_other_columns = F)
    
    file_name <- add_postfix(paste0(prefix, "dmr_candidates.combined.known.all_columns"), !is.null(CONTROL_TISSUE), paste0(".",CONTROL_TISSUE, "_controls"))
    make_bed_file_regions(dmr_candidates.combined.known, file_name, store_other_columns = T)
    
    
    file_name <-  add_postfix(paste0(prefix, "found_among_known.combined"), !is.null(CONTROL_TISSUE), paste0(".",CONTROL_TISSUE, "_controls"))
    tmp <- save_found_among_known(known_to_evaluate, found_among_known_to_evaluate, DMR_description_to_evaluate, file_name, CONTROL_TISSUE)
  }
 
  bump_log <- NULL
  if (fill_log)
  {
    bump_log[["exclude_anchm"]] <- bump_log_exclude_anchm
    bump_log[["with_anchm"]] <- bump_log_with_anchm
  }
  
  return(list(dmr_candidates.with_anchm,
      dmr_candidates.exclude_anchm,
      dmr_candidates.combined,
      found_among_known))
}

