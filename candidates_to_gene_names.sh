FILENAME=$1

if [[ $FILENAME != *.bed ]]; then
	FILENAME=${FILENAME}.bed
fi

head -n 1 ${FILENAME} > header
header=`cat header`
if [[ $header == "chr	start	end"* ]]; then
	tail -n +2 ${FILENAME} > no_header.bed
else
	cp ${FILENAME} no_header.bed
	echo 'chr start end' > header 
fi

cat header | awk -F '\t' -v OFS="\t" '{$4="ucsc_gene"; $5="ucsc_type"; print $0}' > 1.bed
mv 1.bed header

bedtools intersect -bed -wa -wb -a no_header.bed -b Annotations/RefSeq_promoters.with_annotation.with_gene_names.bed > ${FILENAME%.bed}.with_promoters.bed
bedtools intersect -bed -wa -wb -a no_header.bed -b Annotations/RefSeqGenes.with_gene_names.bed > ${FILENAME%.bed}.with_genes.bed
bedtools intersect -bed -wa -wb -a no_header.bed -b Annotations/enhancers.with_annotation.bed > ${FILENAME%.bed}.with_enhancers.bed

cat ${FILENAME%.bed}.with_genes.bed ${FILENAME%.bed}.with_promoters.bed ${FILENAME%.bed}.with_enhancers.bed > ${FILENAME%.bed}.with_annotation.bed
cat ${FILENAME%.bed}.with_annotation.bed | awk -F '\t' -v OFS="\t" '{gene=$(NF-1); type=$NF; $4=gene; $5=type;$(NF-4)=$(NF-3)=$(NF-2)=$(NF-1)=$NF=""; print $0}' > 1.bed
sort 1.bed | uniq > ${FILENAME%.bed}.with_annotation.bed
cat ${FILENAME%.bed}.with_annotation.bed | awk '{print $1"\t"$2"\t"$3}' | uniq > ${FILENAME%.bed}.found_annotation.bed
bedtools intersect -v -a no_header.bed -b ${FILENAME%.bed}.found_annotation.bed > ${FILENAME%.bed}.without_annotation.bed
cat ${FILENAME%.bed}.without_annotation.bed  | awk '{print $0"\tNA\tNA\tNA\tNA\tNA"}' | awk -F '\t' -v OFS="\t" '{gene=$(NF-1); type=$NF; $4=gene; $5=type;$(NF-2)=$(NF-3)=$(NF-4)=$(NF-1)=$NF=""; print $0}' > 1.bed 
cat 1.bed >> ${FILENAME%.bed}.with_annotation.bed
bedtools sort -i ${FILENAME%.bed}.with_annotation.bed > 1.bed
cat 1.bed | awk '{print substr($1,4), $0}' | sort -k1,1n |  awk '{for (i=2; i<NF; i++) printf $i "\t"; print $NF}' > 2.bed
cat  header 2.bed > ${FILENAME%.bed}.with_annotation.bed
rm ${FILENAME%.bed}.with_promoters.bed
rm ${FILENAME%.bed}.with_genes.bed
rm ${FILENAME%.bed}.with_enhancers.bed
rm ${FILENAME%.bed}.found_annotation.bed
rm ${FILENAME%.bed}.without_annotation.bed
rm no_header.bed
rm 1.bed





if [ 0 == 1 ]; then 
	cd Annotations

	cat RefSeq_promoters.bed | awk '{print $0"\t"substr($4, 1, match($4, "_up")-1) }' | less > RefSeq_promoters.with_annotation.bed
	cat RefSeq_promoters.with_annotation.bed | awk '{print $1"\t"$2"\t"$3"\t"$6"\t"$7}' > 1.bed
	mv 1.bed RefSeq_promoters.with_annotation.bed
	#cat RefSeqGenes.bed | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$6"\t"$7"\t"$8}' > 1.bed
	#mv 1.bed RefSeqGenes.bed
	#cat RefSeqIDtoName.bed | awk '{print $2"\t"$13}' > 1.bed
	#tail -n +2 1.bed > RefSeqIDtoName.bed

	sort -k 4 RefSeqGenes.bed > RefSeqGenes.sorted.bed
	sort RefSeqIDtoName.bed > RefSeqIDtoName.sorted.bed

	join -1 4 -2 1 RefSeqGenes.bed RefSeqIDtoName.bed > RefSeqGenes.with_gene_names.bed
	join -1 5 -2 1 RefSeq_promoters.with_annotation.bed RefSeqIDtoName.bed > RefSeq_promoters.with_annotation.with_gene_names.bed

	cat RefSeqGenes.with_gene_names.bed | uniq > 1.bed
	mv 1.bed RefSeqGenes.with_gene_names.bed

	cat RefSeq_promoters.with_annotation.with_gene_names.bed | uniq > 1.bed
	mv 1.bed RefSeq_promoters.with_annotation.with_gene_names.bed

	cat RefSeq_promoters.with_annotation.with_gene_names.bed | awk '{print $2"\t"$3"\t"$4"\t"$1"\t"$6}' > 1.bed
	mv 1.bed RefSeq_promoters.with_annotation.with_gene_names.bed
	cat RefSeqGenes.with_gene_names.bed | awk '{print $2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$1}' > 1.bed
	mv 1.bed RefSeqGenes.with_gene_names.bed

	bedtools sort -i RefSeqGenes.with_gene_names.bed > 1.bed
	mv 1.bed RefSeqGenes.with_gene_names.bed

	bedtools sort -i RefSeq_promoters.with_annotation.with_gene_names.bed > 1.bed
	mv 1.bed RefSeq_promoters.with_annotation.with_gene_names.bed


	cat RefSeq_promoters.with_annotation.with_gene_names.bed | awk '{print $1"\t"$2"\t"$3"\t"$5}' > 1.bed
	mv 1.bed RefSeq_promoters.with_annotation.with_gene_names.bed
	cat RefSeqGenes.with_gene_names.bed | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$7}' > 1.bed
	mv 1.bed RefSeqGenes.with_gene_names.bed

	cat RefSeqGenes.with_gene_names.bed | uniq > 1.bed
	mv 1.bed RefSeqGenes.with_gene_names.bed

	cat RefSeq_promoters.with_annotation.with_gene_names.bed | uniq > 1.bed
	mv 1.bed RefSeq_promoters.with_annotation.with_gene_names.bed

	cat RefSeq_promoters.with_annotation.with_gene_names.bed | awk '{print $0"\tpromoter"}' > 1.bed
	mv 1.bed RefSeq_promoters.with_annotation.with_gene_names.bed

	cat RefSeqGenes.with_gene_names.bed | awk '{print $1"\t"$2"\t"$3"\t"$5}' > 1.bed
	mv 1.bed RefSeqGenes.with_gene_names.bed

	cat RefSeqGenes.with_gene_names.bed | awk '{print $0"\tgene"}' > 1.bed
	mv 1.bed RefSeqGenes.with_gene_names.bed

	bedtools sort -i RefSeqGenes.with_gene_names.bed > RefSeqGenes.with_gene_names.sorted.bed


	#mv RefSeq_promoters.with_annotation.with_gene_names.bed Annotations/
	#mv RefSeqGenes.with_gene_names.bed Annotations/

	# Vista enhancer database
	bedtools sort -i enhancers.bed > enhancers.sorted.bed
	bedtools closest -d -a enhancers.sorted.bed -b RefSeqGenes.with_gene_names.sorted.bed > enhancers.with_annotation.bed
	cat enhancers.with_annotation.bed | awk '{gsub(/200/, "negative_enhancer", $5); gsub(/900/, "positive_enhancer", $5); print $1"\t"$2"\t"$3"\t"$9"\t"$5"\t"$11}' > 1.bed
	mv 1.bed enhancers.with_annotation.bed

	cat enhancers.with_annotation.bed | awk -F '\t' '{ if ($6 <= 100000) {print $0}}' > 1.bed
	mv 1.bed enhancers.with_annotation.bed

	cut  -f 1-5 enhancers.with_annotation.bed > 1.bed
	mv 1.bed enhancers.with_annotation.bed
fi