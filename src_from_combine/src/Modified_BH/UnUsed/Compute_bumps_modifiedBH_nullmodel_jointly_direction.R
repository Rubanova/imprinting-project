setwd("/dupa-filer/yulia/imprinting_project/imprinting_R")
#setwd("/Users/yulia/Documents/imprinting_project")

#library(bumphunter)
library(doParallel)
library(reshape2)
for (file in list.files("bumphunter/R", full.names=T))
  source(file)

load("saved_data/for_bump_hunting.RData")
cl <- makeCluster(detectCores()-1)
registerDoParallel(cl)

CUTOFF <- 0.05
DIR <- "saved_data/modified_bump_hunting_results/"

#FILENAME <- "bumphunter_results.modified.permutations_controls_as_null.005.RData"
#FILENAME <- "bumphunter_results.modified.permutations_no_covariates.005.RData"
#FILENAME <- "bumphunter_results.modified.controls_as_null_no_permutations.005.RData"
FILENAME <- "bumphunter_results.modified.nullmodel_jointly_direction.005.RData"

exclude_anchm <- 1
bumps_with_modifiedBH <- vector("list",2)
bumps_with_modifiedBH[[1]] <- c(0)
bumps_with_modifiedBH[[2]] <- c(0)
bumps_with_modifiedBH[[3]] <- c(0)
names(bumps_with_modifiedBH) <- c("mat", "pat", "joined")
for (chr in names(beta_filtered.splitted))
{
  print(paste("Processing chr", chr, "..."))
  current_chr.data <- beta_filtered.splitted[[chr]][,-1]
  current_chr.annot <- beta_filtered.annot.splitted[[chr]]
  
  mat_indices <- which(alleles[,1] == "mat" & (alleles[,2] == chr | alleles[,2] == 0))
  if (exclude_anchm)
  {
    pat_indices <- which(alleles[,1] == "pat" & (alleles[,2] == chr | alleles[,2] == 0))
  } else
  {
    pat_indices <- which((alleles[,1] == "pat" | alleles[,1] == "anchm") & (alleles[,2] == chr | alleles[,2] == 0))
  }
  
  control_indices <- setdiff(setdiff(which(sample_description$Cell_Type == "Blood"), mat_indices), pat_indices)
  
  samples.maternal <- sample_names[mat_indices]
  samples.paternal <- sample_names[pat_indices]
  samples.control <- sample_names[control_indices]
  
  design_mat <- rbind(matrix(1, nrow=length(mat_indices), ncol=1), matrix(0, nrow=length(pat_indices), ncol=1), matrix(-1, nrow=length(control_indices), ncol=1))
  design_pat <- rbind(matrix(0, nrow=length(mat_indices), ncol=1), matrix(1, nrow=length(pat_indices), ncol=1), matrix(-1, nrow=length(control_indices), ncol=1))
  design <- cbind(design_mat, design_pat)
  
  design_controls <- rbind(matrix(0, nrow=length(mat_indices), ncol=1), matrix(0, nrow=length(pat_indices), ncol=1), matrix(1, nrow=length(control_indices), ncol=1))
  
  beta_filtered.dmr_only <- beta_filtered.dmr_descr[!is.na(beta_filtered.dmr_descr$Imprinted_DMR),]
  control_distribution = melt(beta_filtered.dmr_only[,-1][,control_indices])[,2]
  
  #To compute p-values: pickCutoff=T, B=1000
  tab <- MultiTargetBumphunter(as.matrix(current_chr.data[,c(mat_indices, pat_indices, control_indices)]), design=design, 
                               coef=c(1,2), chr=current_chr.annot$CHR, pos=current_chr.annot$MAPINFO, pickCutoff=T, B=1, maxGap = 500, cutoff=CUTOFF,
                               nullmodel_coef = which(design_controls > 0), computePValuesJointly= T, 
                               bumpDirections = list(c(1,-1), c(-1,1)), SamplesToDetermineDirection=which(design_controls > 0),
                               SamplesContraintedByDistribution = NULL,
                               distribution = NULL)
                               
  #bumps_with_modifiedBH[["mat"]] <- rbind(bumps_with_modifiedBH[["mat"]], tab$table[[1]])
  #bumps_with_modifiedBH[["pat"]] <- rbind(bumps_with_modifiedBH[["pat"]], tab$table[[2]])
  bumps_with_modifiedBH[["joined"]] <- rbind(bumps_with_modifiedBH[["joined"]], tab$table)
}
#bumps_with_modifiedBH[["mat"]] <- bumps_with_modifiedBH[["mat"]][-1,]
#bumps_with_modifiedBH[["pat"]] <- bumps_with_modifiedBH[["pat"]][-1,]
bumps_with_modifiedBH[["joined"]] <- bumps_with_modifiedBH[["joined"]][-1,]

dir.create(DIR, showWarnings = F)
save(bumps_with_modifiedBH, file=paste0(DIR,FILENAME))






# load(paste0(DIR, "bumphunter_results.modified.permutations_controls_as_null.005.RData"))
# bumps_with_modifiedBH.permutations_controls_as_null <- bumps_with_modifiedBH[[1]]
# 
# load(paste0(DIR, "bumphunter_results.modified.permutations_no_covariates.005.RData"))
# bumps_with_modifiedBH.permutations_no_covariates <- bumps_with_modifiedBH[[1]]
# 
# load(paste0(DIR, "bumphunter_results.modified.controls_as_null_no_permutations.005.RData"))
# bumps_with_modifiedBH.controls_as_null_no_permutations <- bumps_with_modifiedBH[[1]]
# 
# bumps.permutations_controls_as_null <- paste(bumps_with_modifiedBH.permutations_controls_as_null$chr, bumps_with_modifiedBH.permutations_controls_as_null$start, bumps_with_modifiedBH.permutations_controls_as_null$end)
# bumps.permutations_no_covariates <- paste(bumps_with_modifiedBH.permutations_no_covariates$chr, bumps_with_modifiedBH.permutations_no_covariates$start, bumps_with_modifiedBH.permutations_no_covariates$end)
# intersection <- intersect(bumps.permutations_controls_as_null, bumps.permutations_no_covariates)
# 
# bumps.permutations_no_covariates.indices <- bumps.permutations_no_covariates %in% intersection
# bumps.permutations_controls_as_null.indices <- bumps.permutations_controls_as_null %in% intersection
# 
# intersection.permutations_controls_as_null <- bumps_with_modifiedBH.permutations_controls_as_null
# intersection.permutations_no_covariates <- bumps_with_modifiedBH.permutations_no_covariates
# 
# View(cbind(bumps_with_modifiedBH.permutations_controls_as_null, bumps_with_modifiedBH.permutations_no_covariates))
