setwd("/dupa-filer/yulia/imprinting_project/imprinting_R")
#setwd("/Users/yulia/Documents/imprinting_project")

ptm_whole <- proc.time()

suppressPackageStartupMessages(library("argparse"))
parser <- ArgumentParser()

parser$add_argument("--dir-output", default="saved_data/modified_bump_hunting_results/", dest="DIR_OUTPUT")
parser$add_argument("--tissue", default="Blood", dest="CONTROL_TISSUE")

args <- parser$parse_args()
CONTROL_TISSUE <- args$CONTROL_TISSUE

DIR <- args$DIR_OUTPUT
if (substr(DIR,nchar(DIR),nchar(DIR)) != "/")
  DIR <- paste0(DIR,"/")
DIR <- paste0(DIR, CONTROL_TISSUE, "/")

#library(bumphunter)
library(doParallel)
library(reshape2)
for (file in list.files("bumphunter/R", full.names=T))
  source(file)

source("src/helper_functions.R")

load("saved_data/for_bump_hunting.RData")

sample_description$Cell_Type <- sapply(sample_description$Cell_Type, toString)
sample_description$Cell_Type[grepl("BA-*", sample_description$Cell_Type)] <- "Brain"
tissues <- sample_description$Cell_Type
tissue_types <- unique(tissues[sample_description$Diagnosis == "Control"])
tissue_types <- tissue_types[-which(tissue_types == "Urine")]

if (!(CONTROL_TISSUE %in% tissue_types))
  stop(paste0("Tissue type is invalid: ", CONTROL_TISSUE))

MODEL_NAME <- "nullmodel_permutations_diff_between_means"
  
cl <- makeCluster(15, outfile=paste0("log/Compute_bumps_modifiedBH.threads.", MODEL_NAME))
registerDoParallel(cl)

CUTOFF <- 0.05

#FILENAME <- "bumphunter_results.modified.permutations_controls_as_null.005.RData"
#FILENAME <- "bumphunter_results.modified.permutations_no_covariates.005.RData"
#FILENAME <- "bumphunter_results.modified.controls_as_null_no_permutations.005.RData"
FILENAME <- paste0("bumphunter_results.modified.", MODEL_NAME, ".005.RData")
if (file.exists(paste0(DIR,FILENAME)))
  stop(paste("File", paste0(DIR,FILENAME), "already exists"))

print(paste0("Performing bump hunting for tissue ", CONTROL_TISSUE, ", model name: ", MODEL_NAME, ". Output directory:", DIR))

exclude_anchm <- 1
bumps_with_modifiedBH <- vector("list",2)
bumps_with_modifiedBH[[1]] <- c(0)
bumps_with_modifiedBH[[2]] <- c(0)
bumps_with_modifiedBH[[3]] <- c(0)
names(bumps_with_modifiedBH) <- c("mat", "pat", "joined")
for (chr in names(beta_filtered.splitted))
{
  print(paste("Processing chr", chr, "..."))
  current_chr.data <- beta_filtered.splitted[[chr]][,-1]
  current_chr.annot <- beta_filtered.annot.splitted[[chr]]
  
  indices_list <- get_sample_indices(alleles, chr, length(sample_names), exclude_anchm, CONTROL_TISSUE, sample_description)
  mat_indices <- indices_list$mat_indices
  pat_indices <- indices_list$pat_indices
  sperm_indices <- indices_list$sperm_indices
  control_indices <- indices_list$control_indices

  samples.maternal <- sample_names[mat_indices]
  samples.paternal <- sample_names[pat_indices]
  samples.control <- sample_names[control_indices]
  
  design_mat <- rbind(matrix(1, nrow=length(mat_indices), ncol=1), matrix(0, nrow=length(pat_indices), ncol=1), matrix(-1, nrow=length(control_indices), ncol=1))
  design_pat <- rbind(matrix(0, nrow=length(mat_indices), ncol=1), matrix(1, nrow=length(pat_indices), ncol=1), matrix(-1, nrow=length(control_indices), ncol=1))
  design <- cbind(design_mat, design_pat)
  
  design_controls <- rbind(matrix(0, nrow=length(mat_indices), ncol=1), matrix(0, nrow=length(pat_indices), ncol=1), matrix(1, nrow=length(control_indices), ncol=1))
  
  beta_filtered.dmr_only <- beta_filtered.dmr_descr[!is.na(beta_filtered.dmr_descr$Imprinted_DMR),]
  control_distribution = melt(beta_filtered.dmr_only[,-1][,control_indices])[,2]
  
  #To compute p-values: pickCutoff=T, B=1000
  tab <- MultiTargetBumphunter(as.matrix(current_chr.data[,c(mat_indices, pat_indices, control_indices)]), design=design, 
                               coef=c(1,2), chr=current_chr.annot$CHR, pos=current_chr.annot$MAPINFO, pickCutoff=T, B=100, maxGap = 500, cutoff=CUTOFF,
                               nullmodel_coef = which(design_controls > 0), computePValuesJointly= F)
                               
  bumps_with_modifiedBH[["mat"]] <- rbind(bumps_with_modifiedBH[["mat"]], tab$table[[1]])
  bumps_with_modifiedBH[["pat"]] <- rbind(bumps_with_modifiedBH[["pat"]], tab$table[[2]])
  #bumps_with_modifiedBH[["joined"]] <- rbind(bumps_with_modifiedBH[["joined"]], tab$table)
}
bumps_with_modifiedBH[["mat"]] <- bumps_with_modifiedBH[["mat"]][-1,]
bumps_with_modifiedBH[["pat"]] <- bumps_with_modifiedBH[["pat"]][-1,]
#bumps_with_modifiedBH[["joined"]] <- bumps_with_modifiedBH[["joined"]][-1,]

dir.create(DIR, showWarnings = F, recursive=T)
save(bumps_with_modifiedBH, file=paste0(DIR,FILENAME))

print("Saved!")
print("Total execution time:")
print(proc.time() - ptm_whole)




# load(paste0(DIR, "bumphunter_results.modified.permutations_controls_as_null.005.RData"))
# bumps_with_modifiedBH.permutations_controls_as_null <- bumps_with_modifiedBH[[1]]
# 
# load(paste0(DIR, "bumphunter_results.modified.permutations_no_covariates.005.RData"))
# bumps_with_modifiedBH.permutations_no_covariates <- bumps_with_modifiedBH[[1]]
# 
# load(paste0(DIR, "bumphunter_results.modified.controls_as_null_no_permutations.005.RData"))
# bumps_with_modifiedBH.controls_as_null_no_permutations <- bumps_with_modifiedBH[[1]]
# 
# bumps.permutations_controls_as_null <- paste(bumps_with_modifiedBH.permutations_controls_as_null$chr, bumps_with_modifiedBH.permutations_controls_as_null$start, bumps_with_modifiedBH.permutations_controls_as_null$end)
# bumps.permutations_no_covariates <- paste(bumps_with_modifiedBH.permutations_no_covariates$chr, bumps_with_modifiedBH.permutations_no_covariates$start, bumps_with_modifiedBH.permutations_no_covariates$end)
# intersection <- intersect(bumps.permutations_controls_as_null, bumps.permutations_no_covariates)
# 
# bumps.permutations_no_covariates.indices <- bumps.permutations_no_covariates %in% intersection
# bumps.permutations_controls_as_null.indices <- bumps.permutations_controls_as_null %in% intersection
# 
# intersection.permutations_controls_as_null <- bumps_with_modifiedBH.permutations_controls_as_null
# intersection.permutations_no_covariates <- bumps_with_modifiedBH.permutations_no_covariates
# 
# View(cbind(bumps_with_modifiedBH.permutations_controls_as_null, bumps_with_modifiedBH.permutations_no_covariates))
