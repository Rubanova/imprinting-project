# Single-ended sequencing for petronis lab cyclic methylation experiment
# with failed r2s

set -eu
set -o pipefail

suffix=.fastq.gz
srcdir="/home/buske/projects/lct/src/bismark"
refdir="/data/buske/lct/ref/bismark"
#trimmomatic="java -Xmx2048m -jar ~buske/src/tools/trimmomatic/Trimmomatic-0.32/trimmomatic-0.32.jar"
trim_galore="/dupa-filer/yulia/Trim_galore/trim_galore"
bismarkdir="/filer/tools/bismark/bismark_v0.12.3"
bowtie="/filer/tools/bowtie/bowtie-1.0.1"
adapters="/data/buske/lct/ref/adapters.fa"
qsub=qsub
tmp=/dupa-filer/yulia/tmp

function trim_sample {
    if [[ ! -s "${dir}/${name}*_val*.fq.gz" ]]; then
	local script=${dir}/pipeline-trim.sh
	cat > $script <<EOF
#!/usr/bin/env bash

#$ -l h_vmem=6G
#$ -V
#$ -S /bin/bash
#$ -N "${name}.trim"
#$ -e "$logdir"
#$ -o "$logdir"
#$ -l hostname="supa*"
#$ -l h_rt=12:00:00
#$ -l s_rt=11:00:11

set -eu

cd ${tmp}

cat ${dir}/${name}_s1_R1.fastq.gz ${dir}/${name}_s2_R1.fastq.gz > ${dir}/${name}_R1.fastq.gz
cat ${dir}/${name}_s1_R2.fastq.gz ${dir}/${name}_s2_R2.fastq.gz > ${dir}/${name}_R2.fastq.gz

# Paired-end trimming
$trim_galore --rrbs --output_dir /dupa-filer/yulia/tmp --path_to_cutadapt /home/yulia/anaconda/bin/cutadapt --paired --trim1 ${dir}/${name}_R1.fastq.gz ${dir}/${name}_R2.fastq.gz


mv ${name}_R*_val_*.fq.gz ${dir}/
EOF

	$qsub $script
    fi
}

function align {
    if [[  ! -e "${dir}/${name}.${assembly}_pe.sam" ]]; then
	local script=${dir}/pipeline-align-${assembly}.sh
	cat > $script <<EOF
#!/usr/bin/env bash

#$ -l h_vmem=16G
#$ -V
#$ -S /bin/bash
#$ -hold_jid "${name}.trim"
#$ -N "${name}.align.${assembly}"
#$ -e "$logdir"
#$ -o "$logdir"
#$ -l hostname="supa*"
#$ -l h_rt=12:00:00
#$ -l s_rt=11:00:11

set -eu

cd ${tmp}

#${bismarkdir}/bismark --path_to_bowtie $bowtie/ --output_dir . --temp_dir . --basename ${name}.${assembly} -n 3  --unmapped  $ref -1 ${dir}/${name}_R1_val_1.fq.gz -2 ${dir}/${name}_R2_val_2.fq.gz

${bismarkdir}/bismark --non_directional --path_to_bowtie $bowtie/ --output_dir . --temp_dir . --basename ${name}.${assembly} -X 1000  --unmapped  $ref -1 ${dir}/${name}_R1_val_1.fq.gz -2 ${dir}/${name}_R2_val_2.fq.gz

mv ${name}.${assembly}* ${dir}/
EOF
	$qsub $script
    fi
}

function coverage {
    if [[ ! -s "${dir}/${name}.${assembly}.merged.cov" ]]; then
	local script=${dir}/pipeline-coverage-${assembly}.sh
	cat > $script <<EOF
#!/usr/bin/env bash

#$ -l h_vmem=8G
#$ -V
#$ -S /bin/bash
#$ -hold_jid "${name}.align.${assembly}"
#$ -N "${name}.cov.${assembly}"
#$ -e "$logdir"
#$ -o "$logdir"
#$ -l hostname="supa*"
#$ -l h_rt=12:00:00
#$ -l s_rt=11:00:11

set -eu
set -o pipefail

cd ${tmp}

# Generate any missing coverage files
if [[ ! -s "${dir}/${name}.${assembly}_pe.bismark.cov" ]]; then
    extra_args=""
    if [[ ! -s "${name}.${assembly}_pe.bedGraph" ]]; then
   	 ${bismarkdir}/bismark_methylation_extractor \${extra_args} --paired-end --comprehensive --merge_non_CpG --report ${dir}/${name}.${assembly}_pe.sam
    fi
    ${srcdir}/mybismark2bedGraph --CX_context --buffer_size 4G -o ${name}.${assembly}_pe.bedGraph {,Non_}CpG_context_${name}.${assembly}_pe.txt
    mv -v ${name}.${assembly}_pe.bedGraph ${dir}/
    mv -v ${name}.${assembly}_pe.bismark.cov ${dir}/
  fi

EOF
	$qsub $script
    fi
}

function process_dir {
    # Set some globals that most functions will need to use
    assembly="$1"
    dir="$2"

    name=$(basename ${dir})

    logdir="${dir}/log"
    mkdir -pv "$logdir"
    
    ref="$refdir/$assembly"

    # Step 1: trim
    trim_sample

    #Step 2: align with bismark
    align

    # Step 3: measure methylation coverage
    coverage
}
