library("NMF")
nmf.options(grid.patch=TRUE)
setwd("/Users/yulia/Documents/imprinting_project/RRBS/Zymo_alignment")

source("../../src/colorcode.R")
source("../../src/utils.R")
library(reshape2)
library(ggplot2)
attach(mtcars)
source("../../src/helper_functions.R")

THRESHOLD <- 10
DATA_START_INDEX <- 4
NUMBER_OF_SAMPLES <- 41
MAGIC_NUMBER <- -0.000001
#RRBS <- read.delim("Zymo_methylation.merged.cov")
#header <- read.delim("RRBS/RRBS_header.cov")
#colnames(RRBS) <- colnames(header)
#save(RRBS, file="Zymo_methylation.RData")

#load("Zymo_methylation.RData")
load("Zymo_alignment.RData")

colnames(RRBS)[1] <- "CHR"
colnames(RRBS)[2] <- "MAPINFO"
rownames(RRBS) <- paste(RRBS$CHR, RRBS$MAPINFO)
  
RRBS.data <- RRBS[,DATA_START_INDEX:ncol(RRBS)]
  
order <- order(as.numeric(gsub("in273_(.*)_.*", "\\1", colnames(RRBS.data))), rep(c("meth", "unmeth"), 41))
RRBS.data <- RRBS.data[, order]
RRBS <- cbind(RRBS[,1:(DATA_START_INDEX-1)], RRBS.data)


RRBS.mc <- RRBS[, seq(DATA_START_INDEX, ncol(RRBS), 2)]
RRBS.c <- RRBS[, seq(DATA_START_INDEX+1, ncol(RRBS), 2)]

RRBS.coverage <- RRBS.mc + RRBS.c
colnames(RRBS.coverage) <- gsub("(in273_[0-9]*)_[^_]*methyl", "\\1",colnames(RRBS.coverage))

#rows.ok <- apply(RRBS.mc + RRBS.c >= THRESHOLD, 1, sum)/ncol(RRBS.mc) > 0.3
#RRBS <- RRBS[rows.ok,]
#RRBS.data <- RRBS.data[rows.ok,]
#RRBS.mc <- RRBS.mc[rows.ok,]
#RRBS.c <- RRBS.c[rows.ok,]

RRBS_percent_meth <- RRBS.mc / (RRBS.mc + RRBS.c)
colnames(RRBS_percent_meth) <- gsub("(in273_[0-9]*)_[^_]*methyl", "\\1",colnames(RRBS.mc))
RRBS_percent_meth <- cbind(RRBS[,1:(DATA_START_INDEX-1)], RRBS_percent_meth)
RRBS_percent_meth.data <- RRBS_percent_meth[,DATA_START_INDEX:ncol(RRBS_percent_meth)]

save(RRBS, RRBS.mc, RRBS.c, RRBS.data, RRBS.coverage, RRBS_percent_meth, RRBS_percent_meth.data, file="RRBS.RData")

RRBS_annotation <- read.delim("/Users/yulia/Documents/imprinting_project/Annotations/RRBS_sample_annotation.txt")
RRBS_annotation <- RRBS_annotation[,-ncol(RRBS_annotation)]
RRBS_alleles <- determine_allele_type(RRBS_annotation$Diagnosis, RRBS_annotation$UPD_type)




# Validation via sex chromosomes
control_blood_indices <- which(RRBS_annotation$Diagnosis == "Control" & RRBS_annotation$Cell_Type == "Blood")
sex_chr_indices <- RRBS_percent_meth$CHR == 'chrY' | RRBS_percent_meth$CHR == 'chrX'
RRBS_percent_meth.sex_chr <- RRBS_percent_meth[sex_chr_indices,]
RRBS_percent_meth.sex_chr <- RRBS_percent_meth.sex_chr[,c(1:(DATA_START_INDEX-1),control_blood_indices)]
colnames(RRBS_percent_meth.sex_chr)[DATA_START_INDEX:ncol(RRBS_percent_meth.sex_chr)] <- unlist(lapply(RRBS_annotation$Zymo_sample_name[control_blood_indices], toString))
RRBS.mc.sex_chr <- RRBS.mc[sex_chr_indices,]
RRBS.mc.sex_chr <- RRBS.mc.sex_chr[,control_blood_indices]
colnames(RRBS.mc.sex_chr) <- lapply(RRBS_annotation$Zymo_sample_name[control_blood_indices], toString)
RRBS.c.sex_chr <- RRBS.c[sex_chr_indices,]
RRBS.c.sex_chr <- RRBS.c.sex_chr[,control_blood_indices]
colnames(RRBS.c.sex_chr) <- lapply(RRBS_annotation$Zymo_sample_name[control_blood_indices], toString)

chrX_indices <- RRBS_percent_meth.sex_chr$CHR == "chrX"
chrY_indices <- RRBS_percent_meth.sex_chr$CHR == "chrY"

THRESHOLD <- 10
pdf(paste0('Control_sex_chr.threshold', THRESHOLD, ".pdf"), width=15, height=11)

FONT.main = 2
FONT.axis = 1
FONT.lab = 1.3
filtered_indices <- (RRBS.mc.sex_chr[,1] + RRBS.c.sex_chr[,1] > THRESHOLD)

control <- melt(RRBS_percent_meth.sex_chr[filtered_indices & chrX_indices,4])
names(control) <- colnames(RRBS_percent_meth.sex_chr)[4]
names(control) <- paste(names(control), 'chrX' )
boxplot(control, at=1, xlim=c(0, length(colnames(RRBS_percent_meth.sex_chr)[DATA_START_INDEX:ncol(RRBS_percent_meth.sex_chr)])*2), ylim=c(0, 1), 
    main=paste("Methylation is sex chromosomes. Threshold", THRESHOLD),
    ylab="Methylation", show.names=T, yaxt="n",  cex.main=FONT.main, cex.axis=FONT.axis, cex.lab=FONT.lab)
                
control <- melt(RRBS_percent_meth.sex_chr[filtered_indices & chrY_indices,4])
names(control) <- colnames(RRBS_percent_meth.sex_chr)[4]
names(control) <- paste(names(control), 'chrY' )
boxplot(control, add=T, at=2, show.names=T, yaxt="n",  cex.main=FONT.main, cex.axis=FONT.axis, cex.lab=FONT.lab)

for (j in 2:length(colnames(RRBS_percent_meth.sex_chr)[DATA_START_INDEX:ncol(RRBS_percent_meth.sex_chr)]))
{
  filtered_indices <- (RRBS.mc.sex_chr[,j] + RRBS.c.sex_chr[,j] > THRESHOLD)
  
  control <- melt(RRBS_percent_meth.sex_chr[filtered_indices & chrX_indices,3+j])
  names(control) <- colnames(RRBS_percent_meth.sex_chr)[3+j]
  names(control) <- paste(names(control), 'chrX' )
  boxplot(control, add=T, at=(0 + 2*j-1), show.names=T, yaxt="n",  cex.main=FONT.main, cex.axis=FONT.axis, cex.lab=FONT.lab)
  
  control <- melt(RRBS_percent_meth.sex_chr[filtered_indices & chrY_indices,3+j])
  names(control) <- colnames(RRBS_percent_meth.sex_chr)[3+j]
  names(control) <- paste(names(control), 'chrY' )
  boxplot(control, add=T, at=(0 + 2*j), show.names=T, yaxt="n",  cex.main=FONT.main, cex.axis=FONT.axis, cex.lab=FONT.lab)
}


axis(side=2,labels=seq(0,1,0.05),at=seq(0,1,0.05))

dev.off()


THRESHOLD <- 20
for (j in 1:length(colnames(RRBS_percent_meth.sex_chr)[DATA_START_INDEX:ncol(RRBS_percent_meth.sex_chr)]))
{
  filtered_indices <- (RRBS.mc.sex_chr[,j] + RRBS.c.sex_chr[,j] > THRESHOLD)
  
  control <- melt(RRBS_percent_meth.sex_chr[filtered_indices & chrX_indices,3+j])
  names(control) <- colnames(RRBS_percent_meth.sex_chr)[3+j]
  names(control) <- paste(names(control), 'chrX' )
  pdf(paste0(names(control),'.threshold', THRESHOLD, ".pdf"), width=15, height=11)
  hist(unlist(control))
  dev.off()
}



















autosomes <- RRBS_percent_meth$CHR!= 'chrY' & RRBS_percent_meth$CHR != 'chrX' & RRBS_percent_meth$CHR != 'chrM'
RRBS.coverage <- RRBS.coverage[autosomes,]
RRBS_percent_meth <- RRBS_percent_meth[autosomes,]
RRBS_percent_meth.data <- RRBS_percent_meth.data[autosomes,]

save(RRBS.coverage, RRBS_percent_meth, RRBS_percent_meth.data, file="RRBS.autosomes.RData")
  
coverage_report(RRBS.coverage, "RRBS.coverage.report.txt")


#fixInNamespace(scale_colours, "NMF")
scale_colours <- function (mat, col = rainbow(10), breaks = NA) 
{
  mat = as.matrix(mat)
  res <- matrix(scale_vec_colours(as.vector(mat), col = col, 
                                  breaks = breaks), nrow(mat), ncol(mat), dimnames = list(rownames(mat), 
                                                                                          colnames(mat)))
  res[mat == MAGIC_NUMBER] <- 1
  return(res)
}



plot_region(RRBS_percent_meth, "chr11", 2019079, 2024126, 'H19_RRBS_aheatmap.pdf', F)
plot_region(RRBS_percent_meth, "chr11", 2715837, 2722440, 'KCNQ1OT1_RRBS_aheatmap.pdf', F)

plot_region(RRBS_percent_meth, "chr11", 2019079, 2024126, 'H19_RRBS_aheatmap.with_marked_low_coverage.pdf', T, RRBS.coverage, THRESHOLD)
plot_region(RRBS_percent_meth, "chr11", 2715837, 2722440, 'KCNQ1OT1_RRBS_aheatmap.with_marked_low_coverage.pdf', T, RRBS.coverage, THRESHOLD)


dir.create("coverage")
get_coverage_file(RRBS_percent_meth, RRBS.coverage, RRBS_percent_meth.data, suffix="", prefix="coverage/")


threshold.report <- c(0)
for (thres in c(0,2,5,10,15,20))
{
  rows.ok <- apply(RRBS.coverage > thres, 1, sum)/ncol(RRBS.coverage) > 0.3
  threshold.report <- rbind(threshold.report, data.frame(threshold=thres, nrows=length(which(rows.ok))))
}
threshold.report <- threshold.report[-1,]
write.table(threshold.report, file="threshold.report.txt", sep = "\t", quote=F, row.names=F)



rows.ok <- apply(RRBS.coverage > THRESHOLD, 1, sum)/ncol(RRBS.coverage) > 0.3
RRBS.coverage <- RRBS.coverage[rows.ok,]
RRBS_percent_meth <- RRBS_percent_meth[rows.ok,]
RRBS_percent_meth.data <- RRBS_percent_meth.data[rows.ok,]

save(RRBS.coverage, RRBS_percent_meth, RRBS_percent_meth.data, file="RRBS.filtered.RData")




RRBS_types <- unique(paste(RRBS_annotation$Diagnosis, RRBS_annotation$Cell_Type, RRBS_annotation$UPD_type))
RRBS_percent_meth.aggregated.data <- data.frame(matrix(NA, ncol=length(RRBS_types), nrow=nrow(RRBS_percent_meth.data)))
colnames(RRBS_percent_meth.aggregated.data) <- RRBS_types
for (i in 1:length(RRBS_types))
{
  current_type <- which(paste(RRBS_annotation$Diagnosis, RRBS_annotation$Cell_Type, RRBS_annotation$UPD_type) == RRBS_types[i])
  if (length(current_type) == 1)
  {
    RRBS_percent_meth.aggregated.data[,i] = RRBS_percent_meth.data[,current_type]
  } else
  {
    current_meth <- RRBS_percent_meth.data[,current_type]
    current_coverage <- RRBS.coverage[,current_type]
    
    RRBS_percent_meth.aggregated.data[,i] = apply(current_meth * current_coverage, 1, sum, na.rm=T) / apply(current_coverage, 1, sum, na.rm=T)
  }
}
RRBS_percent_meth.aggregated <- cbind(RRBS_percent_meth[,1:(DATA_START_INDEX-1)], RRBS_percent_meth.aggregated.data)

save(RRBS_percent_meth.aggregated, file="RRBS_percent_meth.aggregated.RData")

RRBS_percent_meth.splitted <- split(RRBS_percent_meth, RRBS_percent_meth$CHR)
RRBS_percent_meth.data.splitted <- split(RRBS_percent_meth.data, RRBS_percent_meth$CHR)
RRBS.coverage.splitted <- split(RRBS.coverage, RRBS_percent_meth$CHR)


coverage_report(RRBS.coverage, "RRBS.coverage.filtered.report.txt")


plot_region(RRBS_percent_meth, "chr11", 2019079, 2024126, 'H19_RRBS_aheatmap.filtered.pdf', F)
plot_region(RRBS_percent_meth, "chr11", 2715837, 2722440, 'KCNQ1OT1_RRBS_aheatmap.filtered.pdf', F)

plot_region(RRBS_percent_meth, "chr11", 2019079, 2024126, 'H19_RRBS_aheatmap.filtered.with_marked_low_coverage.pdf', T, RRBS.coverage, THRESHOLD)
plot_region(RRBS_percent_meth, "chr11", 2715837, 2722440, 'KCNQ1OT1_RRBS_aheatmap.filtered.with_marked_low_coverage.pdf', T, RRBS.coverage, THRESHOLD)


#H19_coverage <- RRBS.coverage[H19_indices,]
#THRESHOLD <- 5
#rows.ok <- apply( H19_coverage >= THRESHOLD, 1, sum, na.rm=T)/ncol(H19_coverage) > 0.5
#pdf(paste0('H19_RRBS_aheatmap_filtered_by_coverage.pdf'), width=15, height=11)
#aheatmap(H19_agg_percent_meth[rows.ok,DATA_START_INDEX:ncol(H19_agg_percent_meth)], Rowv=NA, Colv=NA)
#dev.off()


plot_region(RRBS_percent_meth.aggregated, "chr11", 2019079, 2024126, 'H19_RRBS_aheatmap_aggregated.filtered.pdf', F)
plot_region(RRBS_percent_meth.aggregated, "chr11", 2715837, 2722440, 'KCNQ1OT1_RRBS_aheatmap_aggregated.filtered.pdf', F)



#H19_coverage <- RRBS.coverage[H19_indices,]
#THRESHOLD <- 5
#rows.ok <- apply( H19_coverage >= THRESHOLD, 1, sum, na.rm=T)/ncol(H19_coverage) > 0.5
#pdf(paste0('H19_RRBS_aheatmap_aggregated.filtered_by_coverage.pdf'), width=15, height=11)
#aheatmap(H19_agg_percent_meth[rows.ok,DATA_START_INDEX:ncol(H19_agg_percent_meth)], Rowv=NA, Colv=NA)
#dev.off()

get_coverage_file(RRBS_percent_meth, RRBS.coverage, RRBS_percent_meth.data, suffix=paste0(".filtered_threshold", THRESHOLD), "coverage/")


sample_names_RRBS <- paste(RRBS_annotation$Zymo_sample_name,RRBS_annotation$Diagnosis,RRBS_annotation$Cell_Type,RRBS_annotation$UPD_type)

load("/Users/yulia/Documents/imprinting_project/saved_data/for_bump_hunting.RData")
RRBS_percent_meth.data.splitted <- split(RRBS_percent_meth.data, RRBS_percent_meth$CHR)
RRBS_percent_meth.annot.splitted <- split(RRBS_percent_meth[,c("CHR", "MAPINFO")], RRBS_percent_meth$CHR)
names(RRBS_percent_meth.data.splitted) <- gsub("chr([0-9]+)", "\\1", names(RRBS_percent_meth.data.splitted))
names(RRBS_percent_meth.annot.splitted) <- gsub("chr([0-9]+)", "\\1", names(RRBS_percent_meth.annot.splitted))
plot_candidates("heatmaps_candidates", "dmr_candidates.combined.all_columns.final.with_annotation.txt", RRBS_annotation, 
  sample_names_RRBS, RRBS_alleles, RRBS_percent_meth.data.splitted, RRBS_percent_meth.annot.splitted, CONTROL_TISSUE=NULL)
plot_candidates("heatmaps_known_regions", "../../known_imprinted_regions.all_columns.bed", RRBS_annotation, sample_names_RRBS, 
  RRBS_alleles, RRBS_percent_meth.data.splitted, RRBS_percent_meth.annot.splitted, CONTROL_TISSUE=NULL)




if (FALSE)
{
  tmp.H19 <- coverage_table$Imprinted_DMR == "H19"
  tmp.KCNQ1OT1 <- coverage_table$Imprinted_DMR == "KCNQ1OT1"
  
  pdf(paste0('H19_RRBS_boxplot.pdf'), width=15, height=11)
  j <- 1
  sample_meth <- melt(coverage_table[tmp.H19, j + 3]) # !!! Bad indices
  names(sample_meth) <- colnames(RRBS_percent_meth.data)[j]
  boxplot(sample_meth, at=1, xlim=c(0, ncol(RRBS_percent_meth.data)), ylim=c(0, 1), 
          main="H19 methylation in RRBS samples",
          ylab="Methylation", show.names=T, yaxt="n",  cex.main=FONT.main, cex.axis=FONT.axis, cex.lab=FONT.lab)
  
  for (j in 2:ncol(RRBS_percent_meth.data))
  {
    filtered_indices <- (RRBS.mc.sex_chr[,j] + RRBS.c.sex_chr[,j] > THRESHOLD)
    
    sample_meth <- melt(coverage_table[tmp.H19,j])
    names(sample_meth) <- colnames(RRBS_percent_meth.data)[j]
    boxplot(sample_meth, add=T, at=j, show.names=T, yaxt="n",  cex.main=FONT.main, cex.axis=FONT.axis, cex.lab=FONT.lab)
  }
  dev.off()
  
  
  
  pdf(paste0('RRBS/KCNQ1OT1_RRBS_boxplot.pdf'), width=15, height=11)
  j <- 1
  sample_meth <- melt(coverage_table[tmp.KCNQ1OT1,j])
  names(sample_meth) <- colnames(RRBS_percent_meth.data)[j]
  boxplot(sample_meth, at=1, xlim=c(0, ncol(RRBS_percent_meth.data)), ylim=c(0, 1), 
          main="KCNQ1OT1 methylation in RRBS samples",
          ylab="Methylation", show.names=T, yaxt="n",  cex.main=FONT.main, cex.axis=FONT.axis, cex.lab=FONT.lab)
  
  for (j in 2:ncol(RRBS_percent_meth.data))
  {
    filtered_indices <- (RRBS.mc.sex_chr[,j] + RRBS.c.sex_chr[,j] > THRESHOLD)
    
    sample_meth <- melt(coverage_table[tmp.H19,j])
    names(sample_meth) <- colnames(RRBS_percent_meth.data)[j]
    boxplot(sample_meth, add=T, at=j, show.names=T, yaxt="n",  cex.main=FONT.main, cex.axis=FONT.axis, cex.lab=FONT.lab)
  }
  dev.off()
}


















exclude_anchm <- 0
regions_to_verify <- read.delim("dmr_candidates.combined.all_columns.final.bed", header=T)
final_candidates <- read.delim("dmr_candidates.combined.all_columns.final.with_annotation.txt", header=T)
regions_to_verify.id = paste(regions_to_verify$chr, regions_to_verify$start, regions_to_verify$end)
final_candidates.id = paste(final_candidates$chr, final_candidates$start, final_candidates$end)

regions_to_verify <- cbind(regions_to_verify, data.frame(type=NA, ucsc_gene=NA, ucsc_type=NA, CpGIsland=NA, verified_on_values = NA, verified_on_wilcox = NA))
regions_to_verify[regions_to_verify$score != 0,]$type <- "Known"

for (i in 1:nrow(regions_to_verify))
{
  if (regions_to_verify[i,]$score != 0)
    next
  
  if (regions_to_verify.id[i] %in% final_candidates.id)
  {
    tmp_region <- final_candidates[which(final_candidates.id == regions_to_verify.id[i]),]
    regions_to_verify[i,]$type <- "final_candidate"
    regions_to_verify[i,]$ucsc_gene <- toString(tmp_region$ucsc_gene)
    regions_to_verify[i,]$ucsc_type <- toString(tmp_region$ucsc_type)
    regions_to_verify[i,]$CpGIsland <- toString(tmp_region$CpGIsland)
  }
  else {
    regions_to_verify[i,]$type <- "filtered_out"
  }
}
                           
names <- as.character(regions_to_verify$Imprinted_DMR)
indices <- which(regions_to_verify$type == "Known")
names[indices] <- paste("Known", regions_to_verify[indices,]$Imprinted_DMR, sep="_")
indices <- which(regions_to_verify$type == "final_candidate")
names[indices] <- paste("final_candidate", regions_to_verify[indices,]$ucsc_gene, regions_to_verify[indices,]$ucsc_type, sep="_")
indices <- which(regions_to_verify$type == "filtered_out")
names[indices] <- paste("filtered_out", regions_to_verify[indices,]$chr, regions_to_verify[indices,]$start, regions_to_verify[indices,]$end, sep="_")

for (i in 1:nrow(regions_to_verify))
{
  data <- find_cpg_in_table(regions = regions_to_verify[i,], all_cpg = RRBS_percent_meth, names = names[i])
  data <- data[,(DATA_START_INDEX+1):ncol(data)]
  verified_on_values <- TRUE
  verified_on_wilcox <- TRUE
  
  chr_number <- as.numeric(gsub("chr([0-9]*)", "\\1", regions_to_verify[i,"chr"]))
  list[mat_indices, pat_indices,
           sperm_indices, control_indices] <- get_sample_indices(RRBS_alleles, chr_number, nrow(RRBS_annotation), exclude_anchm, CONTROL_TISSUE = NULL, sample_description=NULL)
  
  if (regions_to_verify[i,]$DMR_type == "Paternal")
  {
    maternal_meth <- melt(data[,mat_indices])$value
    if (sum(maternal_meth < 0.2, na.rm=T) / length(maternal_meth[!is.na(maternal_meth)]) < 0.75)
      verified_on_values <- FALSE
    
    paternal_meth <- melt(data[,pat_indices])$value
    if (sum(paternal_meth > 0.8, na.rm=T) / length(paternal_meth[!is.na(paternal_meth)]) < 0.75)
      verified_on_values <- FALSE
    
    control_meth <- melt(data[,control_indices])$value
    if (sum(control_meth < 0.7 & control_meth > 0.3, na.rm=T) / length(control_meth[!is.na(control_meth)]) < 0.75)
      verified_on_values <- FALSE
    
    if (wilcox.test(maternal_meth, y = control_meth, alternative = "less")$p.value > 0.05)
      verified_on_wilcox <- FALSE
    
    if (wilcox.test(paternal_meth, y = control_meth, alternative = "greater")$p.value > 0.05)
      verified_on_wilcox <- FALSE
  }
  
  if (regions_to_verify[i,]$DMR_type == "Maternal")
  {
    maternal_meth <- melt(data[,mat_indices])$value
    if (sum(maternal_meth > 0.8, na.rm=T) / length(maternal_meth[!is.na(maternal_meth)]) < 0.75)
      verified_on_values <- FALSE
    
    paternal_meth <- melt(data[,pat_indices])$value
    if (sum(paternal_meth < 0.2, na.rm=T) / length(paternal_meth[!is.na(paternal_meth)]) < 0.75)
      verified_on_values <- FALSE
    
    control_meth <- melt(data[,control_indices])$value
    if (sum(control_meth < 0.7 & control_meth > 0.3, na.rm=T) / length(control_meth[!is.na(control_meth)]) < 0.75)
      verified_on_values <- FALSE
    
    if (wilcox.test(maternal_meth, y = control_meth, alternative = "greater")$p.value > 0.05)
      verified_on_wilcox <- FALSE
    
    if (wilcox.test(paternal_meth, y = control_meth, alternative = "less")$p.value > 0.05)
      verified_on_wilcox <- FALSE
  }
  
  regions_to_verify[i,]$verified_on_values <- verified_on_values
  regions_to_verify[i,]$verified_on_wilcox <- verified_on_wilcox
}









Check meth values for the same positions in 450k and RRBS
align small region with BLAST
Do analysis by-strand
Trace where CpG from imprinted regions were filtered out
plot coverage genome-wide