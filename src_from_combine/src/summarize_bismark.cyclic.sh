#!/usr/bin/env bash

set -eu
set -o pipefail

#ref=hg19
#rgn=( chr2 136540000 136650000 )
#int1=( chr2 136590819 136594759 )
#enh=( chr2 136608600 136609086 )

ref=hg19

function usage {
    cat <<EOF
Usage: $0 dir_with_results...
EOF
    exit 1
}

if [[ $# -eq 0 ]]; then
    usage
fi

function extract {
    echo -ne "$(grep "$1" $dir/*.${ref}*_${2}_report.txt | cut -f 2 | sed 's/%$//')\t"
}

# Print header
echo -e "#sample,#reads,#mapped,mCpG,mCHG,mCHH,CpG,CHG,CHH,%MappedTo${ref}"

columns=( 
    "Sequence.* analysed in total"
    "alignments with a unique best hit"
    "Total methylated C's in CpG context"
    "Total methylated C's in CHG context"
    "Total methylated C's in CHH context"
    "Total unmethylated C's in CpG context"
    "Total unmethylated C's in CHG context"
    "Total unmethylated C's in CHH context"
    "Mapping efficiency"
)

while [[ $# -ge 1 ]]; do
    dir=$1
    shift
    
    echo -ne "$(basename $dir)"
    for col in "${columns[@]}"; do
	echo -ne ",$(expr $(extract "$col" SE))"
    done

    #id=$(uuidgen)
    #awk -v chr=${rgn[0]} -v start=${rgn[1]} -v end=${rgn[2]} '$1 == chr && $2 >= start && $2 <= end && $4 + $5 >= 100' ${dir}/*.${ref}.merged.cov > $id.rgn
    #echo -ne "\t$(cat $id.rgn | grep CpG | wc -l)"
    #echo -ne "\t$(cat $id.rgn | wc -l)"
    #echo -ne "\t$(cat $id.rgn | awk -v chr=${int1[0]} -v start=${int1[1]} -v end=${int1[2]} '$1 == chr && $2 >= start && $2 <= end' | wc -l)"
    #echo -e "\t$(cat $id.rgn | awk -v chr=${enh[0]} -v start=${enh[1]} -v end=${enh[2]} '$1 == chr && $2 >= start && $2 <= end' | wc -l)"
    #rm -f $id.rgn
    echo ""
done
