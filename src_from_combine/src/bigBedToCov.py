#!/usr/bin/env python

"""
Transform bed file obtained from BigBed file to the format of coeverage file
Cov file format: chr pos cpg methyl unmethyl
"""

import os
import sys
import re

from collections import defaultdict

def convert(filename):
    with open(filename) as ifp:
	cpg = re.match(".*_(C.*)_meth.bed", filename).group(1)


	for line in ifp: 
            line = line.rstrip('\n')
            if not line or line.startswith('#'): continue

            tokens = line.split('\t')
            chrom, start, end, meth  = tokens[0:4]
	    
	    n_m, n_total = re.match("^'(\d+)/(\d+)", meth).groups()
	    n_um = int(n_total) - int(n_m)
	
	    print('\t'.join(map(str, (chrom, start, cpg, n_m, n_um))))

def parse_args(args):
    from argparse import ArgumentParser
    description = __doc__.strip()

    parser = ArgumentParser(description=description)
    parser.add_argument('filename', metavar='BISMARK.COV')

    return parser.parse_args(args)

def main(args=sys.argv[1:]):
    args = parse_args(args)
    convert(**vars(args))

if __name__ == '__main__':
    sys.exit(main())

