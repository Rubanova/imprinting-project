# Merge read pairs and filter reads to ARM sequences

set -eu
set -o pipefail

upd_dirs=( 
    $(ls -1d /dupa-filer/yulia/UPD/UPD_RRBS_data/*)
)

source /dupa-filer/yulia/UPD/src/runbismark.cyclic.sh

pause=1
for dir in "${upd_dirs[@]}"; do
     if [[  -e "/dupa-filer/yulia/tmp/$(basename ${dir}).hg38_pe.bismark.cov" ]]; then 
	     mv -v /dupa-filer/yulia/tmp/$(basename ${dir}).hg38_pe.bismark.cov ${dir}/$(basename ${dir}).hg38.merged.cov
     fi

     if [[ $pause ]]; then
	read -p "Press ENTER to continue..."
	pause=
    fi
done
