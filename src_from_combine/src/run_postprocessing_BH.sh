# the scripts to perform all postprocessing of BH
# scripts should be run sequentially. The dependencies on the jobs are not set

# Take bump hunting results and postprocess. The thresholds for postprocessing are determined through grid search.
./make_scripts_to_run_BH_postprocessing.sh compute_roc_curve_server_beta_2_36_regions.R

# Check if all the directories are present. Each directory corresponds to train/test split.
./check_BH_postprocessing.sh modified_bumphunting_results_fmeasure_2_36_known_regions_controlsd_006 

# Check that all the dirs contain training_results
find modified_bumphunting_results_fmeasure_2_36_known_regions_controlsd_006 -type d '!' -exec test -e "{}/training_results.RData" ';' -print

# Gather results from all train/test splits for each bump hunting setting and compose a unified report
./runRscript.sh Find_DMR.R

# Compose ROC curves for each parameter in postprocessing. Can be run together with Find_DMR.R
./make_scripts_to_run_BH_postprocessing.sh compute_roc_curve_per_parameter.R
