#!/usr/bin/env bash

#$ -l excl=true
#$ -V
#$ -S /bin/bash
#$ -N "modified_bump_hunting_results_search_on_abs_values"
#$ -e /dupa-filer/yulia/UPD/UPD_R/log/modified_bump_hunting_results_search_on_abs_values"
#$ -o /dupa-filer/yulia/UPD/UPD_R/log/modified_bump_hunting_results_search_on_abs_values"
#$ -l hostname="supa*"
#$ -l h_rt=48:00:00
#$ -l s_rt=40:00:11

set -eu

Rscript /dupa-filer/yulia/UPD/UPD_R/src/saved_data/modified_bump_hunting_results_search_on_abs_values/ 

