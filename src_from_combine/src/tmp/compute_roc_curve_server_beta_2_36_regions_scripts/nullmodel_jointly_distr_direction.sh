#!/usr/bin/env bash

#$ -l excl=true
#$ -V
#$ -S /bin/bash
#$ -N "nullmodel_jointly_distr_direction.compute_roc_curve_server_beta_2_36_regions"
#$ -e /dupa-filer/yulia/UPD/UPD_R/log/nullmodel_jointly_distr_direction.compute_roc_curve_server_beta_2_36_regions"
#$ -o /dupa-filer/yulia/UPD/UPD_R/log/nullmodel_jointly_distr_direction.compute_roc_curve_server_beta_2_36_regions"
#$ -l hostname="supa*"
#$ -hold_jid "Compute_bumps_modifiedBH_nullmodel_jointly_distr_direction"
#$ -hold_jid "Compute_bumps_modifiedBH_nullmodel_jointly_distr_direction.with_anchm"

set -eu

Rscript /dupa-filer/yulia/UPD/UPD_R/src/compute_roc_curve_server_beta_2_36_regions.R --modified-bh nullmodel_jointly_distr_direction

