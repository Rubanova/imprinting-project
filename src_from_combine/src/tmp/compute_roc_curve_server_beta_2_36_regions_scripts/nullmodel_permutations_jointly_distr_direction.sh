#!/usr/bin/env bash

#$ -l excl=true
#$ -V
#$ -S /bin/bash
#$ -N "nullmodel_permutations_jointly_distr_direction.group10.compute_roc_curve_server_beta_2_36_regions.joint_search_firm_control_requirements"
#$ -e /dupa-filer/yulia/imprinting_project/imprinting_R/log/nullmodel_permutations_jointly_distr_direction.group10.compute_roc_curve_server_beta_2_36_regions.joint_search_firm_control_requirements"
#$ -o /dupa-filer/yulia/imprinting_project/imprinting_R/log/nullmodel_permutations_jointly_distr_direction.group10.compute_roc_curve_server_beta_2_36_regions.joint_search_firm_control_requirements"
#$ -l hostname="supa*"
#$ -l h_rt=100:00:00
#$ -l s_rt=100:00:11
#$ -hold_jid "Compute_bumps_modifiedBH_nullmodel_permutations_jointly_distr_direction"
#$ -hold_jid "Compute_bumps_modifiedBH_nullmodel_permutations_jointly_distr_direction.with_anchm"

set -eu

Rscript /dupa-filer/yulia/imprinting_project/imprinting_R/src/compute_roc_curve_server_beta_2_36_regions.R --modified-bh --model nullmodel_permutations_jointly_distr_direction --group 10 --dir-output modified_bumphunting_results_fmeasure_2_36_known_regions_joint_search_firm_control_requirements/ --dir-input saved_data/modified_bump_hunting_results_joint_search/

