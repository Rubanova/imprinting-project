#!/usr/bin/env bash

#$ -l excl=true
#$ -V
#$ -S /bin/bash
#$ -N "nullmodel.compute_roc_curve_per_parameter"
#$ -e /dupa-filer/yulia/UPD/UPD_R/log/nullmodel.compute_roc_curve_per_parameter"
#$ -o /dupa-filer/yulia/UPD/UPD_R/log/nullmodel.compute_roc_curve_per_parameter"
#$ -l hostname="supa*"
#$ -hold_jid "Compute_bumps_modifiedBH_nullmodel"
#$ -hold_jid "Compute_bumps_modifiedBH_nullmodel.with_anchm"

set -eu

Rscript /dupa-filer/yulia/UPD/UPD_R/src/compute_roc_curve_per_parameter.R --modified-bh nullmodel

