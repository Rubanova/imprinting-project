#!/usr/bin/env bash

#$ -l excl=true
#$ -V
#$ -S /bin/bash
#$ -N "nullmodel_permutations.group1.compute_roc_curve_per_parameter."
#$ -e /dupa-filer/yulia/UPD/UPD_R/log/nullmodel_permutations.group1.compute_roc_curve_per_parameter."
#$ -o /dupa-filer/yulia/UPD/UPD_R/log/nullmodel_permutations.group1.compute_roc_curve_per_parameter."
#$ -l hostname="supa*"
#$ -l h_rt=100:00:00
#$ -l s_rt=100:00:11

set -eu

Rscript /dupa-filer/yulia/UPD/UPD_R/src/compute_roc_curve_per_parameter.R --modified-bh --model nullmodel_permutations --group 1 --dir-output modified_bumphunting_results_fmeasure_2_36_known_regions_checkpoint_no_per_region_threshold/

