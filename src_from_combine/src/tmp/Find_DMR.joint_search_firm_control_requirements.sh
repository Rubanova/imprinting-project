#!/usr/bin/env bash

#$ -l excl=true
#$ -V
#$ -S /bin/bash
#$ -N "Find_DMR.joint_search_firm_control_requirements"
#$ -e /dupa-filer/yulia/imprinting_project/imprinting_R/log/Find_DMR.joint_search_firm_control_requirements"
#$ -o /dupa-filer/yulia/imprinting_project/imprinting_R/log/Find_DMR.joint_search_firm_control_requirements"
#$ -l hostname="supa*"
#$ -l h_rt=48:00:00
#$ -l s_rt=40:00:11

set -eu

Rscript /dupa-filer/yulia/imprinting_project/imprinting_R/src/Find_DMR.R --dir-input modified_bumphunting_results_fmeasure_2_36_known_regions_joint_search_firm_control_requirements/

