#!/usr/bin/env bash

#$ -l excl=true
#$ -V
#$ -S /bin/bash
#$ -N "Find_DMR.randomized_search_08"
#$ -e /dupa-filer/yulia/UPD/UPD_R/log/Find_DMR.randomized_search_08"
#$ -o /dupa-filer/yulia/UPD/UPD_R/log/Find_DMR.randomized_search_08"
#$ -l hostname="supa*"
#$ -l h_rt=48:00:00
#$ -l s_rt=40:00:11

set -eu

Rscript /dupa-filer/yulia/UPD/UPD_R/src/Find_DMR.R --dir-input modified_bumphunting_results_fmeasure_2_36_known_regions_randomized_search_08/

