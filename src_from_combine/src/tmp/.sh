#!/usr/bin/env bash

#$ -l excl=true
#$ -V
#$ -S /bin/bash
#$ -N ""
#$ -e /dupa-filer/yulia/UPD/UPD_R/log/"
#$ -o /dupa-filer/yulia/UPD/UPD_R/log/"
#$ -l hostname="supa*"
#$ -l h_rt=48:00:00
#$ -l s_rt=40:00:11

set -eu

Rscript /dupa-filer/yulia/UPD/UPD_R/src/--dir-output 

