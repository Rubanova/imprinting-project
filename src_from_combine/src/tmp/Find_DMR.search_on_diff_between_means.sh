#!/usr/bin/env bash

#$ -l excl=true
#$ -V
#$ -S /bin/bash
#$ -N "Find_DMR.search_on_diff_between_means"
#$ -e /dupa-filer/yulia/UPD/UPD_R/log/Find_DMR.search_on_diff_between_means"
#$ -o /dupa-filer/yulia/UPD/UPD_R/log/Find_DMR.search_on_diff_between_means"
#$ -l hostname="supa*"
#$ -l h_rt=48:00:00
#$ -l s_rt=40:00:11

set -eu

Rscript /dupa-filer/yulia/UPD/UPD_R/src/Find_DMR.R --dir-input modified_bumphunting_results_fmeasure_2_36_known_regions_search_on_diff_between_means/

