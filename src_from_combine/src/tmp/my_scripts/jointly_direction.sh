#!/usr/bin/env bash

#$ -l excl=true
#$ -V
#$ -S /bin/bash
#$ -N "jointly_direction.group10.my"
#$ -e /dupa-filer/yulia/UPD/UPD_R/log/jointly_direction.group10.my"
#$ -o /dupa-filer/yulia/UPD/UPD_R/log/jointly_direction.group10.my"
#$ -l hostname="supa*"
#$ -hold_jid "Compute_bumps_modifiedBH_jointly_direction"
#$ -hold_jid "Compute_bumps_modifiedBH_jointly_direction.with_anchm"
#$ -l h_rt=100:00:00
#$ -l s_rt=100:00:11

set -eu

Rscript /dupa-filer/yulia/UPD/UPD_R/src/my.R --modified-bh --model jointly_direction --group 10 

