#!/usr/bin/env bash

#$ -l excl=true
#$ -V
#$ -S /bin/bash
#$ -N "nullmodel_permutations.group10.my"
#$ -e /dupa-filer/yulia/UPD/UPD_R/log/nullmodel_permutations.group10.my"
#$ -o /dupa-filer/yulia/UPD/UPD_R/log/nullmodel_permutations.group10.my"
#$ -l hostname="supa*"
#$ -hold_jid "Compute_bumps_modifiedBH_nullmodel_permutations"
#$ -hold_jid "Compute_bumps_modifiedBH_nullmodel_permutations.with_anchm"
#$ -l h_rt=100:00:00
#$ -l s_rt=100:00:11

set -eu

Rscript /dupa-filer/yulia/UPD/UPD_R/src/my.R --modified-bh --model nullmodel_permutations --group 10 

