#!/usr/bin/env bash

#$ -l excl=true
#$ -V
#$ -S /bin/bash
#$ -N "Find_DMR"
#$ -e /dupa-filer/yulia/UPD/UPD_R/log/Find_DMR"
#$ -o /dupa-filer/yulia/UPD/UPD_R/log/Find_DMR"
#$ -l hostname="supa*"
#$ -l h_rt=48:00:00
#$ -l s_rt=40:00:11

set -eu

Rscript /dupa-filer/yulia/UPD/UPD_R/src/Find_DMR.R

