#!/usr/bin/env bash

#$ -l excl=true
#$ -V
#$ -S /bin/bash
#$ -N "Compute_bumps_modifiedBH_jointly_distr_direction.with_anchm.Lymphoblast"
#$ -e /dupa-filer/yulia/imprinting_project/imprinting_R/log/Compute_bumps_modifiedBH_jointly_distr_direction.with_anchm.Lymphoblast"
#$ -o /dupa-filer/yulia/imprinting_project/imprinting_R/log/Compute_bumps_modifiedBH_jointly_distr_direction.with_anchm.Lymphoblast"
#$ -l hostname="supa*"
#$ -l h_rt=48:00:00
#$ -l s_rt=40:00:11

set -eu

Rscript /dupa-filer/yulia/imprinting_project/imprinting_R/src/Modified_BH/Compute_bumps_modifiedBH_jointly_distr_direction.with_anchm.R --dir-output saved_data/modified_bump_hunting_results_search_on_abs_values/ --tissue Lymphoblast

