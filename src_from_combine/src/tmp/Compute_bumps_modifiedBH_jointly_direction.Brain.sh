#!/usr/bin/env bash

#$ -l excl=true
#$ -V
#$ -S /bin/bash
#$ -N "Compute_bumps_modifiedBH_jointly_direction.Brain"
#$ -e /dupa-filer/yulia/imprinting_project/imprinting_R/log/Compute_bumps_modifiedBH_jointly_direction.Brain"
#$ -o /dupa-filer/yulia/imprinting_project/imprinting_R/log/Compute_bumps_modifiedBH_jointly_direction.Brain"
#$ -l hostname="supa[01]*"
#$ -l h_rt=48:00:00
#$ -l s_rt=40:00:11

set -eu

Rscript /dupa-filer/yulia/imprinting_project/imprinting_R/src/Modified_BH/Compute_bumps_modifiedBH_jointly_direction.R --dir-output saved_data/modified_bump_hunting_results_control_comparison_with_50/ --tissue Brain

