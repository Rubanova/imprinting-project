#!/usr/bin/env bash

#$ -l excl=true
#$ -V
#$ -S /bin/bash
#$ -N "Compute_bumps_modifiedBH_nullmodel"
#$ -e /dupa-filer/yulia/UPD/UPD_R/log/Compute_bumps_modifiedBH_nullmodel"
#$ -o /dupa-filer/yulia/UPD/UPD_R/log/Compute_bumps_modifiedBH_nullmodel"
#$ -l hostname="supa*"
#$ -l h_rt=48:00:00
#$ -l s_rt=40:00:11

set -eu

Rscript /dupa-filer/yulia/UPD/UPD_R/src/Modified_BH/Compute_bumps_modifiedBH_nullmodel.R --dir-output saved_data/modified_bump_hunting_results_search_on_abs_values/

