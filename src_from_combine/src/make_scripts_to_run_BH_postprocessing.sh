#!/bin/bash

# Script to run postprocessing of bump hunting in various configurations

# Usage ./make_scripts_to_run_BH_postprocessing.sh compute_roc_curve_server_beta_2_36_regions.R [--extra-args "argument_for_r_script"; --job-id ...]
# Note that argument for r script should all be in quotes!
# R script names should be given with respect to $current_folder/src/ directory. Output or input directory names should be given with respect to $current_folder directory

rscript=$1

current_folder=/dupa-filer/yulia/imprinting_project/imprinting_R

if [ -z "$rscript" ]; then
    echo "Please provide the name of r script to run"
    exit 1
fi

if [[ $rscript != *.R ]]; then
	echo "The specified file $rscript is not an R script"
fi

extra_args=""
if [[ $2 = "--extra-args" ]]; then
	if [[ -z $3 ]]; then
		echo "Please provide extra arguments for R script"
		exit
	else
		extra_args=$3
	fi
fi

extra_job_id=""
if [[ $4 = "--job-id" ]]; then
        if [[ -z $5 ]]; then
                echo "Please provide extra job id for R script"
                exit
        else
                extra_job_id=$5
        fi
fi

dir=${current_folder}/src/tmp/${rscript%.R}_scripts
logdir=${current_folder}/log
if [[ !  -e "$dir" ]]; then
	mkdir -p $dir
fi

if [[ ! -e "$logdir" ]]; then
	mkdir -p $logdir
fi

#models=("permutations" "nullmodel" "nullmodel_permutations" "jointly_direction" "jointly_distr_direction" "nullmodel_jointly_direction"  "nullmodel_permutations_jointly_direction" "nullmodel_jointly_distr_direction" "nullmodel_permutations_jointly_distr_direction")

#models=("permutations" )
#tissues=("Blood")


models=("permutations" "nullmodel_permutations" "permutations_diff_between_means" "nullmodel_permutations_diff_between_means" "jointly_direction" "jointly_distr_direction"  "nullmodel_permutations_jointly_direction" "nullmodel_permutations_jointly_distr_direction")

tissues=("Blood" "Brain" "Cerebellum" "Kidney" "Liver" "Fibroblast")

for tissue in ${tissues[@]}; do
#for group in `seq 1 5`; do
for group in `seq 1 1`; do   
for model in ${models[@]}; do
	base=$(basename $rscript)
	name=${model}.group${group}.${tissue}.${base%.R}
	if [[ $extra_job_id != "" ]]; then
		name=${name}.${extra_job_id}
	fi
	#name=${model}.${rscript%.R}
	script=${dir}/${model}.${tissue}.group${group}.sh
        
	output_file=$logdir/${name}
	error_file=$logdir/${name}

	if [[ -e "$output_file" ]]; then
		rm ${output_file}
	fi
	if [[ -e "$error_file" ]]; then
		rm ${error_file}
	fi

#$ -l hostname="supa*"
#$ -l hostname="supa[01]*"
#$ -l excl=true

	cat > $script <<EOF
#!/usr/bin/env bash

#$ -V
#$ -S /bin/bash
#$ -N "${name}"
#$ -e ${error_file}"
#$ -o ${output_file}"
#$ -l hostname="supa*"
#$ -l excl=true
#$ -l h_rt=100:00:00
#$ -l s_rt=100:00:11
#$ -hold_jid "Compute_bumps_modifiedBH_${model}.${tissue}"
#$ -hold_jid "Compute_bumps_modifiedBH_${model}.with_anchm.${tissue}"

set -eu

Rscript ${current_folder}/src/${rscript} --modified-bh --model ${model} --group ${group} --tissue ${tissue} ${extra_args}

EOF

        qsub $script
done
done
done

#rm -r ${dir}
