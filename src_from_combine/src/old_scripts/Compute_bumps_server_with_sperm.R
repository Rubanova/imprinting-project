setwd("/dupa-filer/yulia/UPD")
library(bumphunter)
library(doParallel)

load("for_bump_hunting.RData")
cl <- makeCluster(detectCores()-1)
registerDoParallel(cl)

CUTOFF <- 0.05
FILENAME <- "bumphunter_results.0.05.sperm.RData"
exclude_anchm <- 0
sperm_bumps <- c(0)
for (chr in names(beta_filtered.splitted))
{
  print(paste("Processing chr", chr, "..."))
  current_chr.data <- beta_filtered.splitted[[chr]][,-1]
  current_chr.annot <- beta_filtered.annot.splitted[[chr]]
  
  sperm_indices <- which(alleles[,1] == "sperm" & (alleles[,2] == chr | alleles[,2] == 0))
  mat_indices <- which(alleles[,1] == "mat" & (alleles[,2] == chr | alleles[,2] == 0))
  if (exclude_anchm)
  {
    pat_indices <- which(alleles[,1] == "pat" & (alleles[,2] == chr | alleles[,2] == 0))
  } else
  {
    pat_indices <- which((alleles[,1] == "pat" | alleles[,1] == "anchm") & (alleles[,2] == chr | alleles[,2] == 0))
  }
  
  control_indices <- setdiff(setdiff(setdiff(1:length(sample_names), mat_indices), pat_indices), which(alleles[,1] == "anchm" | alleles[,1] == "sperm"))
  
  samples.maternal <- sample_names[mat_indices]
  samples.paternal <- sample_names[pat_indices]
  samples.control <- sample_names[control_indices]
  samples.sperm <- sample_names[sperm_indices]
  
  design <- rbind(matrix(1, nrow=length(sperm_indices), ncol=1), matrix(0, nrow=length(control_indices), ncol=1))
  #To compute p-values: pickCutoff=T, B=1000
  tab <- bumphunter(as.matrix(current_chr.data[,c(sperm_indices, control_indices)]), design=design, coef=1, chr=current_chr.annot$CHR, pos=current_chr.annot$MAPINFO, pickCutoff=T, B=100, maxGap = 500, cutoff=CUTOFF)
  sperm_bumps <- rbind(sperm_bumps, tab$table)

}

sperm_bumps <- sperm_bumps[-1,]

save(sperm_bumps, file=FILENAME)