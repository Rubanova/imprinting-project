setwd("/dupa-filer/yulia/UPD")
library(bumphunter)
library(doParallel)

load("for_bump_hunting.RData")
cl <- makeCluster(detectCores()-1)
registerDoParallel(cl)

CUTOFF <- 0.1
exclude_anchm <- 0
maternal_bumps <- c(0)
paternal_bumps <- c(0)
for (chr in names(beta_filtered.splitted))
{
  print(paste("Processing chr", chr, "..."))
  current_chr.data <- beta_filtered.splitted[[chr]][,-1]
  current_chr.annot <- beta_filtered.annot.splitted[[chr]]
  
  mat_indices <- which(alleles[,1] == "mat" & (alleles[,2] == chr | alleles[,2] == 0))
  if (exclude_anchm)
  {
    pat_indices <- which(alleles[,1] == "pat" & (alleles[,2] == chr | alleles[,2] == 0))
  } else
  {
    pat_indices <- which((alleles[,1] == "pat" | alleles[,1] == "anchm") & (alleles[,2] == chr | alleles[,2] == 0))
  }
  
  control_indices <- setdiff(setdiff(setdiff(1:length(sample_names), mat_indices), pat_indices), which(alleles[,1] == "anchm" | alleles[,1] == "sperm"))
  
  samples.maternal <- sample_names[mat_indices]
  samples.paternal <- sample_names[pat_indices]
  samples.control <- sample_names[control_indices]
  
  design <- rbind(matrix(1, nrow=length(mat_indices), ncol=1), matrix(0, nrow=length(control_indices), ncol=1))
  #To compute p-values: pickCutoff=T, B=1000
  tab <- bumphunter(as.matrix(current_chr.data[,c(mat_indices, control_indices)]), design=design, coef=1, chr=current_chr.annot$CHR, pos=current_chr.annot$MAPINFO, pickCutoff=T, B=100, maxGap = 500, cutoff=CUTOFF)
  maternal_bumps <- rbind(maternal_bumps, tab$table)
  
  design <- rbind(matrix(1, nrow=length(pat_indices), ncol=1), matrix(0, nrow=length(control_indices), ncol=1))
  tab <- bumphunter(as.matrix(current_chr.data[,c(pat_indices, control_indices)]), design=design, coef=1, chr=current_chr.annot$CHR, pos=current_chr.annot$MAPINFO, pickCutoff=T, B=100, maxGap = 500, cutoff=CUTOFF)
  paternal_bumps <- rbind(paternal_bumps, tab$table) 

}

maternal_bumps <- maternal_bumps[-1,]
paternal_bumps <- paternal_bumps[-1,]

save(maternal_bumps, paternal_bumps, file="bumphunter_results.01.RData")