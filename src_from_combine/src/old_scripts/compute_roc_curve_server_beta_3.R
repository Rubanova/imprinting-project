#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)

MODEL_NAME = ""
if (args[1] == "--modified-bh")
{
  MODIFIED_BH <- TRUE
  if (length(args) == 1)
    stop("Model name should be specified")
  
  MODEL_NAME <- args[2]
}
if (MODIFIED_BH)
{
  if (length(args) > 3)
  {
    if (args[3] == "--dir-input")
    {
      DIR_INPUT <- args[4]
    } 
  } else {
    DIR_INPUT <- "saved_data/modified_bump_hunting_results/"
  }
  if (length(args) > 5)
  {
    if (args[5] == "--dir-output")
    {
      DIR_OUTPUT <- args[6]
    } 
  } else {
    DIR_OUTPUT <- "modified_bumphunting_results_fmeasure_3_52_known_regions_controlsd_3_options/"
  }
  if (length(args) > 9)
  {
    if (args[7] == "--prefix" & args[9] == "--postfix")
    {
      FILE_PREFIX <- args[8]
      FILE_POSTFIX <- args[10]
    } 
  } else {
    FILE_PREFIX <- "bumphunter_results.modified."
    FILE_POSTFIX <- ".005.RData"
  }
} else
{
  FILE_PREFIX <- ""
  FILE_POSTFIX <- ""
  DIR_INPUT <- "saved_data/"
  DIR_OUTPUT <- "bumphunting_results/"
  MODEL_NAME <- ""
}




DIR = "/dupa-filer/yulia/UPD/UPD_R"
setwd(DIR)

source("src/helper_functions.R")
source("src/find_bumps.R")
source("src/Compute_candidates_for_various_data_subsets.R")
source("src/Get_final_candidates.R")

library(doParallel)

output_file <- paste0("/dupa-filer/yulia/UPD/UPD_R/tmp.output_", MODEL_NAME)
if (file.exists(output_file))
  file.remove(output_file)
file.create(output_file, showWarnings=F, overwrite=T)

closeAllConnections()
cl <- makeCluster(15, outfile=output_file)
registerDoParallel(cl)
getDoParWorkers()

USE_ONLY_BLOOD_CONTROLS <- TRUE
USE_ONLY_PAT_MAT_DIFF <- FALSE


if (!MODIFIED_BH)
{
  print(paste0("Computing parameters with USE_ONLY_BLOOD_CONTROLS=", USE_ONLY_BLOOD_CONTROLS, ", USE_ONLY_PAT_MAT_DIFF=",USE_ONLY_PAT_MAT_DIFF))
} else {
  print(paste0("Computing parameters with MODIFIED_BH=", MODIFIED_BH, " MODEL_NAME=", MODEL_NAME, " FILE_PREFIX=", FILE_PREFIX, " FILE_POSTFIX=", FILE_POSTFIX,
               " DIR_INPUT=", DIR_INPUT, " DIR_OUTPUT=", DIR_OUTPUT))
}
#negatives_amount <- cbind(as.numeric(names(beta_filtered.splitted)), lapply(beta_filtered.splitted, nrow))[order(as.numeric(names(beta_filtered.splitted))),]
#save(DMR_description_train_test, known, negatives_amount, file="saved_data/DMR_description_train_test_and_known.RData")
load("saved_data/DMR_description_train_test_and_known.RData")

# Find possible train/test splits
known_imprinted_regions <- read.delim("known_imprinted_regions.all_columns.bed")
known_imprinted_regions2 <- cbind(as.numeric(gsub("chr[d]?", "\\1", known_imprinted_regions$chr)), unlist(lapply(known_imprinted_regions$ucsc_gene, toString)), known_imprinted_regions[,2:3])
colnames(known_imprinted_regions2) <- c("chr", "Imprinted_DMR", "start", "end")



if (FALSE)
{
  chr_split = 12

  known_imprinted_regions <- read.delim("known_imprinted_regions.all_columns.bed")
  tmp <- cbind(as.numeric(gsub("chr[d]?", "\\1", known_imprinted_regions$chr)), lapply(known_imprinted_regions$ucsc_gene, toString))
  
  known_after_split <- known[names(known) %in% unlist(tmp[unlist(tmp[,1]) >= chr_split,2])]
  known_before_split <- known[names(known) %in% unlist(tmp[unlist(tmp[,1]) < chr_split,2])]
  
  sets_of_train_test <- list()
  sets_of_train_test[[1]] <- list(train_positives = known_before_split, test_positives = known_after_split,
                                  train_negatives = 1:(chr_split-1), test_negatives = chr_split:22)
  sets_of_train_test[[2]] <- list(train_positives = known_after_split, test_positives = known_before_split,
                                  train_negatives = chr_split:22, test_negatives = 1:(chr_split-1))
  names(sets_of_train_test) <- c("before_split", "after_split")
}


# parameters for grid search
if(0)
{
  parameters <- c(0)
  for (p.value in c(0.05))
    for (value in c(0.1))
      for (L in c(1))
        for (merge_interval in c(2000))
          for (PAT_UPD_BETA_DIFF in c(0.1, 0.13))
            for (PAT_WG_UPD_BETA_DIFF in c(0.05, 0.07))
              for (CONTROL_SD in c(0.05, 0.07, 0.1))
                for (CONTROL_VARIENCE_FROM_50 in c(0.15))
                  for (CONTROL_VARIENCE_FROM_50_INDIVIDUAL in c(0.25))
                    for (SUM_BETA_DIFF_THRESHOLD in c(0.2, 0.25, 0.3))
                      for (CPG_AMOUNT_THRESHOLD in c(2, 3))
                        for (MAT_BETA_DIFF in c(0.03, 0.05))
                          for (PAT_BETA_DIFF in c(0.07, 0.1, 0.13))                         
                          {
                            tmp <- data.frame(p.value = p.value, value = value, L = L, 
                                              merge_interval = merge_interval, SUM_BETA_DIFF_THRESHOLD = SUM_BETA_DIFF_THRESHOLD, CPG_AMOUNT_THRESHOLD = CPG_AMOUNT_THRESHOLD,
                                              MAT_BETA_DIFF = MAT_BETA_DIFF, PAT_BETA_DIFF = PAT_BETA_DIFF, CONTROL_SD = CONTROL_SD,
                                              PAT_UPD_BETA_DIFF = PAT_UPD_BETA_DIFF, PAT_WG_UPD_BETA_DIFF = PAT_WG_UPD_BETA_DIFF, CONTROL_VARIENCE_FROM_50 = CONTROL_VARIENCE_FROM_50,
                                              CONTROL_VARIENCE_FROM_50_INDIVIDUAL = CONTROL_VARIENCE_FROM_50_INDIVIDUAL)
                            
                            parameters <- rbind(parameters, tmp)
                          }
  parameters <- parameters[-1,]
  save(parameters, file="saved_data/parameters_roc_curve.RData")

  beta_filtered.annot.splitted.reduced <- beta_filtered.annot.splitted
  for (i in 1:length(beta_filtered.annot.splitted))
  {
    beta_filtered.annot.splitted.reduced[[i]] <- beta_filtered.annot.splitted[[i]][,c("CHR", "MAPINFO")]
  }
  beta_filtered.annot.reduced <- beta_filtered.annot[,c("CHR", "MAPINFO")]
  beta_filtered.annot.CHR <- beta_filtered.annot$CHR
  
  save(PREFIX, sample_names, beta_filtered, known_train_test, DMR_description_train_test,
       beta_filtered.annot.reduced, beta_filtered.annot.splitted.reduced, beta_filtered.annot.CHR,
       alleles, sample_description, file="saved_data/For_computing_roc_curve.updated2.RData")
}




load("saved_data/parameters_roc_curve.RData")
load("saved_data/For_computing_roc_curve.updated2.52_regions.RData")
load("saved_data/sets_of_train_test.reduced.RData")
sets_of_train_test <- sets_of_train_test.reduced


PREFIX <- DIR_OUTPUT
dir.create(PREFIX, showWarnings=F)

prefix <- PREFIX
if (USE_ONLY_PAT_MAT_DIFF)
{
  prefix <- paste0(prefix, "pat_mat_differences/")
} else if (MODIFIED_BH)
{
  prefix <- paste0(prefix, MODEL_NAME, "/")
} else {
  prefix <- paste0(prefix, "pat_mat_compared_to_controls/")
}
already_computed <- list.files(prefix)




results_train_test_sets <- list()
for (set_index in 1:length(sets_of_train_test))
{
  if (names(sets_of_train_test)[set_index] %in% already_computed)
  {
    print(paste0("Skipping set ", set_index))
    next
  }

  print(paste0("Computing for set ", set_index))
  
  set <- sets_of_train_test[[set_index]]
  
  known_train <- set$train_positives
  known_test <- set$test_positives
  
  DMR_description_train <- DMR_description_train_test[DMR_description_train_test$Imprinted_DMR %in% names(known_train),]
  DMR_description_train$Imprinted_DMR <- droplevels(DMR_description_train$Imprinted_DMR)
  DMR_description_test <- DMR_description_train_test[DMR_description_train_test$Imprinted_DMR %in% names(known_test),]
  DMR_description_test$Imprinted_DMR <- droplevels(DMR_description_test$Imprinted_DMR)
  
  ptm <- proc.time()
  results_training <- foreach(k = (1:nrow(parameters)), .combine=rbind) %dopar% 
  { 
    print(paste("Thread", k, "started"))
    #print(paste("Thread", k, "loaded data"))
    
    beta_filtered.splitted <- split(beta_filtered, beta_filtered.annot.CHR)
    if (names(beta_filtered.splitted)[1] == "")
    {
      beta_filtered.splitted <- beta_filtered.splitted[-1]
    }
    
    source("src/helper_functions.R")
    source("src/find_bumps.R")
    source("src/Compute_candidates_for_various_data_subsets.R")
    source("src/Get_final_candidates.R")
    
    current_params <- parameters[k,]
    params <- list()
    params[names(unlist(current_params))] <- unlist(current_params)
    
    index <- k
    dir.create(prefix, showWarnings = F, recursive = T)
    
    list[dmr_candidates.with_anchm,
         dmr_candidates.exclude_anchm,
         dmr_candidates.combined,
         found_among_known_to_evaluate,
         sensitivity_tpr,
         specificity_tnr,
         fdr, bump_log] <- 
      compute_candidates_for_various_data_subsets(beta_filtered[beta_filtered.annot.reduced$CHR %in% set$train_negatives,],
                                                  known_to_evaluate = known_train, DMR_description_to_evaluate = DMR_description_train, 
                                                  known_train_test, DMR_description_train_test, file, "tmp/", index, 
                                                  beta_filtered.splitted[names(beta_filtered.splitted) %in% set$train_negatives], beta_filtered.annot.reduced[beta_filtered.annot.reduced$CHR %in% set$train_negatives,],
                                                  beta_filtered.annot.splitted.reduced[names(beta_filtered.annot.splitted.reduced) %in% set$train_negatives], 
                                                  alleles, 
                                                  USE_ONLY_BLOOD_CONTROLS, USE_ONLY_PAT_MAT_DIFF, sample_description, params, save_results = FALSE, sample_names, chrs = set$train_negatives,
                                                  MODIFIED_BH = MODIFIED_BH, FILE_PREFIX = FILE_PREFIX, FILE_POSTFIX = FILE_POSTFIX, MODEL_NAME = MODEL_NAME, DIR_INPUT = DIR_INPUT)
    
    print(paste("Thread", k, "finished"))
    
    data.frame(sensitivity_tpr = sensitivity_tpr, specificity_tnr = specificity_tnr, fdr = fdr,
                      bumps_amount = nrow(dmr_candidates.combined), params)
  }
  print(proc.time() - ptm)
  
  print("Training complete")
  
  index <- ""
  beta_filtered.splitted <- split(beta_filtered, beta_filtered.annot.CHR)
  if (names(beta_filtered.splitted)[1] == "")
  {
    beta_filtered.splitted <- beta_filtered.splitted[-1]
  }
  
  precision <- 1 - results_training$fdr
  recall <- results_training$sensitivity_tpr
  beta <- 3 # Recall is weighted higher than precision
  f_measure <- (1 + beta^2) * (precision * recall) / (beta^2 * precision + recall)
  optimal_parameters <- parameters[which.max(f_measure),]
  
  # Evaluation how diff parameters influence results
  evaluation_results <- data.frame(matrix(NA, nrow=4, ncol=ncol(parameters)))
  colnames(evaluation_results) <- colnames(parameters)
  rownames(evaluation_results) <- c("sensitivity_tpr_diff", "sensitivity_tpr_mean", "fdr_diff", "fdr_mean")
  for (param in names(parameters))
  {
    # Fix all values other than "param". Take all rows from parameter variable there all parameters other than "param" are equal to optimal
    considered_parameter_sets <- apply(parameters[,setdiff(names(parameters), param)], 1, is_optimal, optimal_parameters = optimal_parameters, params_to_compare = setdiff(names(parameters), param))
    considered_parameter_sets <- results_training[considered_parameter_sets,]
    
    evaluation_results["sensitivity_tpr_diff",param] <- max(considered_parameter_sets$sensitivity_tpr, na.rm=T) - min(considered_parameter_sets$sensitivity_tpr, na.rm=T)
    evaluation_results["sensitivity_tpr_mean",param] <- mean(considered_parameter_sets$sensitivity_tpr, na.rm=T)
    
    evaluation_results["fdr_diff",param] <- max(considered_parameter_sets$fdr, na.rm=T) - min(considered_parameter_sets$fdr, na.rm=T)
    evaluation_results["fdr_mean",param] <- mean(considered_parameter_sets$fdr, na.rm=T)
  }
  
  params <- as.list(unlist(optimal_parameters))
  
  prefix <- PREFIX
  if (USE_ONLY_PAT_MAT_DIFF)
  {
    prefix <- paste0(prefix, "pat_mat_differences/")
  } else if (MODIFIED_BH)
  {
    prefix <- paste0(prefix, MODEL_NAME, "/")
  } else {
    prefix <- paste0(prefix, "pat_mat_compared_to_controls/")
  }
  prefix <- paste0(prefix, names(sets_of_train_test)[set_index], "/")
  
  dir.create(prefix, showWarnings = F, recursive = T)
  
  
  list[dmr_candidates.with_anchm,
       dmr_candidates.exclude_anchm,
       dmr_candidates.combined,
       found_among_known_to_evaluate,
       sensitivity_tpr,
       specificity_tnr,
       fdr, bump_log] <- compute_candidates_for_various_data_subsets(beta_filtered[beta_filtered.annot.reduced$CHR %in% set$test_negatives,],
                                                           known_to_evaluate = known_test, DMR_description_to_evaluate = DMR_description_test, 
                                                           known_train_test, DMR_description_train_test, file, prefix, index, 
                                                           beta_filtered.splitted[names(beta_filtered.splitted) %in% set$test_negatives], beta_filtered.annot.reduced[beta_filtered.annot.reduced$CHR %in% set$test_negatives,],
                                                           beta_filtered.annot.splitted.reduced[names(beta_filtered.annot.splitted.reduced) %in% set$test_negatives], 
                                                           alleles, 
                                                           USE_ONLY_BLOOD_CONTROLS, USE_ONLY_PAT_MAT_DIFF, sample_description, params, save_results = TRUE, sample_names, chrs = set$test_negatives,
                                                           MODIFIED_BH = MODIFIED_BH, FILE_PREFIX = FILE_PREFIX, FILE_POSTFIX = FILE_POSTFIX, MODEL_NAME = MODEL_NAME, DIR_INPUT = DIR_INPUT)
  
  print(paste("Get final candidates"))
  
  list[dmr_candidates.with_anchm.final,
       dmr_candidates.without_anchm.final,
       dmr_candidates.combined.final] <- get_final_candidates(USE_ONLY_BLOOD_CONTROLS, prefix, USE_ONLY_PAT_MAT_DIFF)
  
  results_train_test_sets[[set_index]] <- list(optimal_parameters = optimal_parameters, evaluation_results = evaluation_results,
                                               sensitivity_tpr = sensitivity_tpr,
                                               specificity_tnr = specificity_tnr,
                                               fdr = fdr,
                                               found_among_known_to_evaluate = found_among_known_to_evaluate, 
                                               dmr_candidates.combined = dmr_candidates.combined,
                                               dmr_candidates.combined.final = dmr_candidates.combined.final,
                                               results_training = results_training)
  names(results_train_test_sets)[set_index] <- names(sets_of_train_test)[set_index]
  
  print("Saving...")
  file_name <- "training_results"
#   file_name <- paste0(file_name, names(results_train_test_sets)[set_index])
#   if (USE_ONLY_PAT_MAT_DIFF)
#   {
#     file_name <- paste0(file_name,".pat_vs_mat")
#   } else if(MODIFIED_BH)
#   {
#     file_name <- paste0(file_name, ".", MODEL_NAME)
#   }
  file_name <- paste0(file_name,".RData")
  save(results_train_test_sets, file=paste0(prefix, file_name))
}

prefix <- PREFIX
if (USE_ONLY_PAT_MAT_DIFF)
{
  prefix <- paste0(prefix, "pat_mat_differences/")
} else if (MODIFIED_BH)
{
  prefix <- paste0(prefix, MODEL_NAME, "/")
} else {
  prefix <- paste0(prefix, "pat_mat_compared_to_controls/")
}
file_name <- "training_results.RData"
save(results_train_test_sets, file=paste0(prefix, file_name))


print("Saved!")

stopCluster(cl)
closeAllConnections()
