
set -eu
set -o pipefail

coverage_dirs=( 
    $(ls -1d /dupa-filer/petronis/round2/CR_project_merged_lanes/CR-*/* )
)

tmp="/tmp/yulia"

srcdir="/dupa-filer/petronis/src"

for dir in ${coverage_dirs[@]}; do

    coverage_file=( 
    $(ls -1d ${dir}/*.cov)
    )

    name=$(basename ${dir})
    filename=$( basename ${coverage_file})

    logdir="${dir}/log"
    mkdir -pv "$logdir"
    l=${#filename}
 
    touch ${tmp}/runR_${name}.sh
    script=${tmp}/runR_${name}.sh
    cat > $script <<EOF

#!/usr/bin/env bash

#$ -l h_vmem=16G
#$ -V
#$ -S /bin/bash
#$ -hold_jid "${name}.runR"
#$ -e "$logdir"
#$ -o "$logdir"
#$ -l hostname="supa*"


set -eu
set -o pipefail

Rscript ${srcdir}/methylation_to_binary.R ${filename} ${dir}

EOF
        qsub $script
done


