#!/bin/bash

# Script to run the computation of bump hunting for different cobfigurations

# Usage: ./make_scripts_to_run_modified_BH.sh [--extra-args "arguments_for_r_script" --job-id extra_id ] `ls Modified_BH/Compute_bumps_modifiedBH*`
# Note that arguments for R script should be quoted and form one string
# R script names should be given with respect to $current_folder/src/ directory. Output or input directory names should be given with respect to $current_folder directory

current_folder=/dupa-filer/yulia/imprinting_project/imprinting_R

extra_args=""
rscripts=("$@")
if [[ $1 = "--extra-args" ]]; then
        if [[ -z $2 ]]; then
                echo "Please provide extra argumets for R script"
                exit
        else
                extra_args=$2
		rscripts=("${@:3}")
        fi
fi

extra_job_id=""
if [[ $3 = "--job-id" ]]; then
        if [[ -z $4 ]]; then
                echo "Please provide extra argumets for R script"
                exit
        else
                extra_job_id=$4
		rscripts=("${@:5}")
        fi
fi

if [ -z "$1" ]; then
    echo "Please provide the name of r script to run"
    exit 1
fi

for s in ${rscripts[@]}; do
	if [[ $s != *.R ]]; then
		echo "The specified file $s is not an R script"
	fi
done

dir=${current_folder}/src/tmp
logdir=${current_folder}/log
if [[ !  -e "$dir" ]]; then
	mkdir $dir
fi

if [[ ! -e "$logdir" ]]; then
	mkdir $logdir
fi

for sc in ${rscripts[@]}; do
	name=$(basename ${sc})
	name=${name%.R}
	if [[ $extra_job_id != "" ]]; then
              name=${name}.${extra_job_id}
	fi
	script=${dir}/${name}.sh
        
	output_file=$logdir/${name}
	error_file=$logdir/${name}

	if [[ -e ${output_file} ]]; then
		rm ${output_file}
	fi
	if [[ -e ${error_file} ]]; then
		rm ${error_file}
	fi

#$ -l hostname="supa*"

	cat > $script <<EOF
#!/usr/bin/env bash

#$ -l excl=true
#$ -V
#$ -S /bin/bash
#$ -N "${name}"
#$ -e ${error_file}"
#$ -o ${output_file}"
#$ -l hostname="supa[01]*"
#$ -l h_rt=48:00:00
#$ -l s_rt=40:00:11

set -eu

Rscript ${current_folder}/src/${sc} ${extra_args}

EOF
        qsub $script
done

#rm -r ${dir}
