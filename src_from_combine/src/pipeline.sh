# the scripts to perform BH and postprocessing of BH
# scripts should be run sequentially. The dependencies on the jobs are not set
# R script names should be given with respect to src/ directory. Output or input directory names should be given with respect to imprinting_R directory

tissues=("Blood" "Brain" "Cerebellum" "Kidney" "Liver" "Fibroblast")
for tissue in ${tissues[@]}; do 
	./make_scripts_to_run_modified_BH.sh --extra-args "--dir-output saved_data/modified_bump_hunting_results_final/ --tissue ${tissue}" --job-id ${tissue} `ls Modified_BH/Compute_bumps_modifiedBH*`; 
done

# Take bump hunting results and postprocess. The thresholds for postprocessing are determined through grid search.
./make_scripts_to_run_BH_postprocessing.sh compute_BH_postprocessing.R --extra-args "--dir-output modified_bumphunting_results_fmeasure_2_36_known_regions_final/ --dir-input saved_data/modified_bump_hunting_results_final/" --job-id joint_search

# Check if all the directories are present. Each directory corresponds to train/test split.
./check_BH_postprocessing.sh  modified_bumphunting_results_fmeasure_2_36_known_regions_final

# Check that all the dirs contain training_results
find  modified_bumphunting_results_fmeasure_2_36_known_regions_final -type d '!' -exec test -e "{}/training_results.RData" ';' -print

# Gather results from all train/test splits for each bump hunting setting and compose a unified report
tissues=("Blood" "Brain" "Cerebellum" "Kidney" "Liver" "Fibroblast")
for tissue in ${tissues[@]}; do
	./runRscript.sh Gather_final_results.R --extra-args "--dir-input modified_bumphunting_results_fmeasure_2_36_known_regions_final/ --dir-bumps saved_data/modified_bump_hunting_results_final/ --tissue ${tissue}" --job-id joint_search.${tissue};
done

# Compose ROC curves for each parameter in postprocessing. Can be run together with Find_DMR.R
./make_scripts_to_run_BH_postprocessing.sh compute_roc_curve_per_parameter.R
