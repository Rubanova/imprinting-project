# Merge read pairs and filter reads to ARM sequences

set -eu
set -o pipefail

upd_dirs=( 
    $(ls -1d /dupa-filer/yulia/UPD/UPD_RRBS_data/Zymo_alignment/*/)
)

source /dupa-filer/yulia/UPD/src_Zymo_alignment/runbismark.cyclic.sh

pause=1
for dir in "${upd_dirs[@]}"; do 
    process_dir hg19 "${dir}"
    if [[ $pause ]]; then
	read -p "Press ENTER to continue..."
	pause=
    fi
done
