# Merge read pairs and filter reads to ARM sequences

set -eu
set -o pipefail

fastq_files=( 
    $(ls -1d /dupa-filer/yulia/UPD/UPD_RRBS_data/Zymo_alignment/* )
)

pause=1

for file in "${fastq_files[@]}"; do 
    new_dir=${file%%.*}
    if [[  ! -e "${new_dir}" ]]; then
	mkdir ${new_dir}
    fi
    mv ${file}  ${new_dir}
    if [[ $pause ]]; then
	read -p "Press ENTER to continue..."
	pause=
    fi
done
