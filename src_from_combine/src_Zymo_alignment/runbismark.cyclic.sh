# Single-ended sequencing for petronis lab cyclic methylation experiment
# with failed r2s

set -eu
set -o pipefail

srcdir="/home/buske/projects/lct/src/bismark"
refdir="/data/buske/lct/ref/bismark"
bismarkdir="/filer/tools/bismark/bismark_v0.12.3"
adapters="/data/buske/lct/ref/adapters.fa"
qsub=qsub
tmp=/dupa-filer/yulia/tmp

function coverage {
    if [[ ! -s "${dir}/${name}.merged.cov" ]]; then
	local script=${dir}/pipeline-coverage-${assembly}.sh
	cat > $script <<EOF
#!/usr/bin/env bash

#$ -l h_vmem=8G
#$ -V
#$ -S /bin/bash
#$ -N "${name}.cov.${assembly}"
#$ -e "$logdir"
#$ -o "$logdir"
#$ -l hostname="supa*"
#$ -l h_rt=12:00:00
#$ -l s_rt=11:00:11

set -eu
set -o pipefail

cd ${tmp}

if [[ ! -s "${dir}/${name}.sorted.bam" ]]; then
	samtools view -bf2 ${dir}/${name}.bam > ${name}.unsorted.bam
	samtools sort -n ${name}.unsorted.bam ${name}.sorted
	mv ${name}.sorted.bam ${dir}/${name}.sorted.bam
fi

# Generate any missing coverage files
if [[ ! -s "${dir}/${name}.bismark.cov" ]]; then
    extra_args="--genome_folder=/data/buske/lct/ref/bismark/hg19 --cytosine_report"
    if [[ ! -s "${name}.bedGraph" ]]; then
   	 ${bismarkdir}/bismark_methylation_extractor \${extra_args} --paired-end --comprehensive --merge_non_CpG --report ${dir}/${name}.sorted.bam
    fi
    ${srcdir}/mybismark2bedGraph --CX_context --buffer_size 4G -o ${name}.sorted.bedGraph {,Non_}CpG_context_${name}.sorted.txt
    mv -v ${name}.sorted.bedGraph ${dir}/${name}.bedGraph
    mv -v ${name}.sorted.bismark.cov ${dir}/${name}.bismark.cov
    mv -v ${name}.sorted.CpG_report.txt ${dir}/${name}.CpG_report.txt
   fi

EOF
	$qsub $script
    fi
}

function process_dir {
    # Set some globals that most functions will need to use
    assembly="$1"
    dir="$2"

    name=$(basename ${dir})

    logdir="${dir}/log"
    mkdir -pv "$logdir"
    
    ref="$refdir/$assembly"

    coverage
}
